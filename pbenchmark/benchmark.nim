import times

template benchmark*(name: static string, body: untyped): untyped =
  block:
    let start = cpuTime()
    body
    let elapsed = cpuTime() - start
    echo "CPU Time for ", name, ": ", elapsed, "s"
