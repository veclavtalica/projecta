## Index iteration

import ./benchmark
import pcommon/intrinsics

var arr = create(array[250_0000, uint])
var data = createU(array[1000_0000, uint])

template work(index: int, item: var uint): untyped =
  item = data[index * 4] + data[index * 4 + 1] + data[index * 4 + 2] + data[
      index * 4 + 3]

template workFar(index: int, item: var uint): untyped =
  item = data[index * 8] + data[index * 8 + 1] + data[index * 8 + 2] + data[
      index * 8 + 3] + data[index * 8 + 4] + data[index * 8 + 5] + data[index *
          4 + 6] + data[index * 8 + 7]

benchmark "index + item":
  for _ in 0..<100:
    for i, v in arr[].mpairs:
      work i, v

benchmark "index + item with prefetch":
  # Conclusions:
  # It's not faster in general case, resulting in about:
  #   CPU Time for index + item: 1.573s
  #   CPU Time for index + item with prefetch: 1.616s
  # But interestingly, if other processes are working at the same time, execution time changes drastically:
  #   CPU Time for index + item: 4.31s
  #   CPU Time for index + item with prefetch: 3.762999999999999s
  # So it looks like prefetches are beneficial when fighting for cache is expected.
  for _ in 0..<100:
    for i, v in arr[].mpairs:
      prefetch data[i * 4].addr, writeExpected = false, locality = High
      work i, v

benchmark "index + item with threads (openmp)":
  for _ in 0..<100:
    for i in 0||(arr[].len - 1):
      work i, arr[i]

benchmark "index + item with threads (openmp) with prefetch":
  for _ in 0..<100:
    for i in 0||(arr[].len - 1):
      prefetch data[i * 4].addr, writeExpected = false, locality = High
      work i, arr[i]

benchmark "index + item with threads":
  proc worker(start, ending: int) =
    for _ in 0..<100:
      for i in start..<ending:
        work i, arr[i]

  proc workerThread(pack: tuple[start, ending: int]) {.thread.} =
    worker pack.start, pack.ending

  var thread: Thread[tuple[start, ending: int]]
  thread.createThread workerThread, (0, arr[].len div 2)
  worker arr[].len div 2, arr[].len
  joinThread thread

benchmark "index + item with threads with prefetch":
  # Conclusions:
  # Threads within the same process doesnt benefit from prefetch in this case:
  #   CPU Time for index + item with threads: 1.456s
  #   CPU Time for index + item with threads with prefetch: 1.470000000000001s
  proc worker(start, ending: int) =
    for _ in 0..<100:
      for i in start..<ending:
        prefetch data[i * 4].addr, writeExpected = false, locality = High
        work i, arr[][i]

  proc workerThread(pack: tuple[start, ending: int]) {.thread.} =
    worker pack.start, pack.ending

  var thread: Thread[tuple[start, ending: int]]
  thread.createThread workerThread, (0, arr[].len div 2)
  worker arr[].len div 2, arr[].len
  joinThread thread

benchmark "far index + item":
  for _ in 0..<100:
    for i, v in arr[].toOpenArray(0, arr[].len div 2).mpairs:
      workFar i, v

benchmark "far index + item with prefetch":
  # Conclusions:
  # When there's substantial distance between accessible memory - prefetch provides benefits.
  #   CPU Time for far index + item: 1.625s
  #   CPU Time for far index + item with prefetch: 1.59s
  for _ in 0..<100:
    for i, v in arr[].toOpenArray(0, arr[].len div 2).mpairs:
      prefetch data[i * 8].addr, writeExpected = false, locality = High
      work i, v
