## Simple but efficient uniform grid tilemap.
##
## Things to consider:
## Interleaving VBO is potentially worse, as we dont ever will modify vertices, but only UV
## For that we could allocate single VBO, but use half for static vertices, while modifying latter half UVs,
## but it will screw 'usage' hint. It's better having those VBOs separate.
## Also we could double buffer the VBO we want to frequently modify, it will make synchronization easier for GPU.
## https://www.khronos.org/opengl/wiki/Vertex_Specification_Best_Practices
##

# todo: Allow multiple graphics api implementations
# todo: Use glDrawRangeElements as we know the maximum value of indices.
# todo: We could fetch GL_MAX_ELEMENTS_VERTICES and GL_MAX_ELEMENTS_INDICES to know the preferred draw call data size
#       and then separate the single draw call into multiples.
# todo: Allow changes to atlas, with immediate update

from std/strutils import unindent

import pcommon/options
import pcommon/types/[allocator, color, vec]
import pgraphics/gl/[gl, texture, shader, buffer]
import pprocgen/mesh/grid

type
  Tilemap*[A: Allocator, C: AnyColor] = object
    atlas: Texture[C]
      ## Texture that could be uniformly divided.
    tileDimensions: Vec2[uint]
      ## Size of a single tile, in texels.
      # todo: Abstract atlas texture with tile dimensions
    vertexBuffer: GlServerBuffer[Vec2[GLfloat]]
    indexBuffer: GlServerBuffer[GLuint]
      # todo: Use GLshort if its applicable, we can switch to GLuint dynamically if it's needed
      # todo: Use shared index buffer with other grid like renderers
    uvBuffer: GlBuffer[A, Vec2[GLushort]]
      # todo: Consider double buffering UV buffer, as it is the thing that's being constantly modified.
    uvFirstChange, uvLastChange: uint32
      # Indexes of cells that were updated between renders, used for reducing data passing demand
      # todo: Implement Range type

const
  vertexShader = """
    #version 120

    attribute vec2 in_position;
    attribute vec2 in_uv;

    uniform vec2 u_position;
    uniform float u_scale;

    varying vec2 var_uv;

    void main() {
      gl_Position = vec4((in_position + u_position) * vec2(u_scale), 0.0, 1.0);
      var_uv = in_uv;
    }

  """.unindent()
  fragmentShader = """
    #version 120

    uniform sampler2d u_atlas;

    varying vec2 var_uv;

    void main() {
      gl_FragColor = texure2D(u_atlas, var_uv);
    }

  """.unindent()

const UniformAtlasTextureUnit = 0.TextureUnit

var program: Shader

# todo: One problem with this strategy is that it's not obvious when we should free these resources
#       Destructor on global scope could also be called after gl context is no longer valid
proc initSharedState =
  ## We intend to reuse resources between different tilemap instances,
  ## so they're kept in global variables that are instanced once
  once:
    program = must initShader(vertexShader, fragmentShader)
  with program:
    program.setUniform "u_atlas", UniformAtlasTextureUnit.toGLenum.GLint

proc initTilemap*[C](allocator: typedesc[Allocator],
                     atlas: sink Texture[C],
                     tileDimensions: Vec2[uint]): Tilemap[allocator, C] =
  initSharedState()
  result.atlas = atlas
  result.tileDimensions = tileDimensions
  result.vertexBuffer = initGlServerBuffer(Vec2[GLfloat], ArrayBuffer, Static)
  result.indexBuffer = initGlServerBuffer(GLuint, ElementBuffer, Static)
  result.uvBuffer = initGlBuffer(allocator, Vec2[GLushort], ArrayBuffer, Dynamic)

proc prepareGrid*(x: var Tilemap, gridDimensions: Vec2[uint]) =
  ## Prepare buffers for certain configuration of window and grid dimensions.
  ## Warning: This invalidates all previously stored information, full tile information should be passed again.
  with x.vertexBuffer:
    x.vertexBuffer.doubleBuffer gridDimensions.area * verticesPerQuad
    x.vertexBuffer.withMapped vertices, WriteOnly:
      generateGridMesh vertices, gridDimensions
  with x.indexBuffer:
    x.indexBuffer.doubleBuffer gridDimensions.area * indicesPerQuad
    x.indexBuffer.withMapped indices, WriteOnly:
      generateGridMeshIndices indices, gridDimensions.area

proc render*(tilemap: Tilemap, windowDimensions: Vec2[uint], scale = 1.0,
    offset = Vec2[float].default) =
  ## Render tilemap to current gl context
  ## Note: It assumes that depth buffer doesnt matter, overwriting the values.
  ##       It's usually desired for 2d rendering. If you need to have 3d, better render tilemaps first,
  ##       as it will fill depth buffer, preventing unneeded fragment pixel passes.
  # todo: Turn off face culling locally
  # todo: We could eliminate nesting of 'with' by using defer blocks
  withDepthFunc GlAlways:
    with program:
      with tilemap.atlas:
        program.validate()

proc `=copy`*[A, C](dest: var Tilemap[A, C], source: Tilemap[A, C]) {.error.}

proc `=destroy`[A, C](x: var Tilemap[A, C]) =
  `=destroy`(x.atlas)
  `=destroy`(x.vertexBuffer)
  `=destroy`(x.uvBuffer)
  `=destroy`(x.indexBuffer)
