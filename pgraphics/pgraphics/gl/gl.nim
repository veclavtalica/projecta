import std/[parseutils, strformat]

import pkg/[opengl]
export opengl

import pcommon/types/[color, vec, rect]
import pcommon/io/log
import pcommon/intrinsics

# https://www.khronos.org/opengl/wiki/Common_Mistakes
# https://www.glprogramming.com/red/appendixb.html

# todo: Test suite for ensuring sections blocks are working
# todo: We need to prevent user from being able to set GL state bypassing this interface
# todo: Name it to something better i guess?
# todo: Should we implement stacks for name binding control? It would allow for easier nesting
#       Or rather make name binding safer, for example, by making them as 'with' operations, that disallow rebinding while other bind is present
#       Nesting is just almost never a good idea and should be discouraged in the first place, as it introduces more gl commands
# todo: Clip panel API
# todo: Near/far plane API

when defined(windows):
  # This symbols could be interpreted by vendor driver as 'Use the most performant device available'
  # https://stackoverflow.com/questions/68469954/how-to-choose-specific-gpu-when-create-opengl-context
  {.emit: """
  __declspec(dllexport) DWORD NvOptimusEnablement = 1;
  __declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;
  """.}

type
  GlPrimitive* = GLboolean|GLbyte|GLubyte|GLshort|GLushort|GLint|GLuint|GLfixed|GLint64|GLuint64 |
    GLsizei|GLenum|GLintptr|GLsizeiptr|GLsync|GLbitfield|GLfloat|GLclampf|GLdouble|GLclampd

  TextureUnit* = distinct uint8

converter toGLenum*(unit: TextureUnit): GLenum =
  GLenum GlTexture0.uint + unit.uint * 2

converter toGLenum*(t: typedesc[GlPrimitive]): GLenum {.compiletime.} =
  when t is GLbyte: cGL_BYTE
  elif t is GLubyte: cGL_UNSIGNED_BYTE
  elif t is GLshort: cGL_SHORT
  elif t is GLushort: cGL_UNSIGNED_SHORT
  elif t is GLint: cGL_INT
  elif t is GLuint: GL_UNSIGNED_INT
  elif t is GLfixed: cGL_FIXED
  # elif t is GLhalf: cGL_HALF_FLOAT
  elif t is GLfloat: cGL_FLOAT
  elif t is GLdouble: cGL_DOUBLE
  else:
    {.fatal: "Unknown GlType to common enum conversion".}

var glVersionPair: tuple[major, minor: int]
var glslVersionPair: tuple[major, minor: int]

proc getValue[T](name: GLenum): T =
  ## GL version: 2.0
  when T is GLint:
    glGetIntegerv(name, result.addr)
  when T is GLuint:
    # GL doesnt provide get for unsigned values
    glGetIntegerv(name, cast[ptr GLint](result.addr))
  else:
    static: doAssert(false)

proc glVersion*: tuple[major, minor: int] =
  once:
    let versionString = $cast[cstring](glGetString(GlVersion))
    doAssert(versionString.len != 0)
    let dot = versionString.find('.')
    doAssert(dot != -1)
    discard parseSaturatedNatural(versionString, glVersionPair.major)
    discard parseSaturatedNatural(captureBetween(versionString, '.', '.', dot),
        glVersionPair.minor)
    ## GL version: 3.0
    # majorVersion = getValue[GLint](GlMajorVersion)
    # minorVersion = getValue[GLint](GlMinorVersion)
  glVersionPair

proc glslVersion*: tuple[major, minor: int] =
  once:
    let versionString = $cast[cstring](glGetString(GlShadingLanguageVersion))
    doAssert(versionString.len != 0)
    let dot = versionString.find('.')
    doAssert(dot != -1)
    discard parseSaturatedNatural(versionString, glslVersionPair.major)
    discard parseSaturatedNatural(captureBetween(versionString, '.', '.', dot),
        glslVersionPair.minor)
  glslVersionPair

proc vendor*: string =
  $cast[cstring](glGetString(GlVendor))

proc rendererName*: string =
  $cast[cstring](glGetString(GlRenderer))

proc init* =
  loadExtensions()

  # It's turned on by default, but we dont really desire it
  glDisable GlDither

  # Depth test is assimed to be on from the start
  glEnable GlDepthTest

  when not defined(release):
    glEnable GlDebugOutput

  # todo: Only do this on hardware that is expected to be affected by this
  # Old ATI card drivers appear to have a bug where mipmaps dont get generated unless this is performed
  glEnable GlTexture2D

  glHint GL_PERSPECTIVE_CORRECTION_HINT, GL_FASTEST

  log.segment("Graphical API",
    fmt"Renderer: {rendererName()}",
    fmt"OpenGL version: {glVersion()}",
    fmt"GLSL version: {glslVersion()}",
    fmt"Vendor: {vendor()}")

proc clear* =
  ## Clear GL depth buffer without touching pixel state.
  ## It's minorly faster to do than clear with color, but you must ensure that screen will be fully written,
  ## otherwise expect artifacts such as in Doom engine when you go out of bounds.
  # todo: Stencil buffer might need to be cleared even if not used if implementation interleaves them.
  # https://community.khronos.org/t/avoid-glclear-gl-color-buffer-bit/65771
  # todo: This post mentions cases where clearing of color bit actually gave performance benefit, we might need to test this.
  glClear(GlDepthBufferBit or GlStencilBufferBit)

proc clear*(color: ColorRgba[float]) =
  var prevColor {.global.}: typeof(color) = typeof(color).default
  if prevColor != color:
    glClearColor(color.red, color.green, color.blue, color.alpha)
    prevColor = color
  # todo: Stencil buffer might need to be cleared even if not used if implementation interleaves them.
  glClear(GlColorBufferBit or GlStencilBufferBit or GlDepthBufferBit)

# todo: How to do it without sdl on pre 3.0 gl?
# proc hasExtension*(ext: static[string]): bool =
  ## Operates on current context
  # if (glVersion().major < 3):
  #   doAssert(glGetStringi != nil)
  # let extensions = getValue[GLuint](GlNumExtensions)
  # for idx in 0..<extensions:
  #   if ext.cstring == cast[cstring](glGetStringi(GlExtensions, idx)):
  #     return true

var
  depthTestIsOn = true
  cullFaceIsOn = false
  currentDepthFunc = GlLess
  currentVertexBuffer = 0.GLuint
  currentElementBuffer = 0.GLuint
  currentTextureBind = 0.GLuint
  currentActiveTexture = 0.TextureUnit
  currentUsedProgram = 0.GLuint

  # Guards that prevent nesting of blocks with certain GL state
  isInTextureBindSection = false
  isInVertexBufferBindSection = false
  isInElementBufferBindSection = false
  isInScissorSection = false
  isInActiveTextureSection = false
  isInUsedProgramSection = false

# todo: Move target setting/restoring logic to separate function? As templating it would potentially produce a lot of binary
template withBindBuffer*(target: GLenum, name: GLuint, body): untyped =
  ## Way to bind buffer object with awareness to currently bound buffer.
  ## It's important as many procedures aren't aware of it, and assume that they *must* bind it,
  ## while quite commonly you have chained commands to a single target.
  case target:
  of GlArrayBuffer:
    if currentVertexBuffer != name:
      assert(not isInVertexBufferBindSection)
      glBindBuffer target, name
      currentVertexBuffer = name
      isInVertexBufferBindSection = true
  of GlElementArrayBuffer:
    if currentElementBuffer != name:
      assert(not isInElementBufferBindSection)
      glBindBuffer target, name
      currentElementBuffer = name
      isInElementBufferBindSection = true
  else:
    doAssert false
    unreachable()

  body

  case target:
  of GlArrayBuffer:
    isInVertexBufferBindSection = false
  of GlElementArrayBuffer:
    isInElementBufferBindSection = false
  else:
    doAssert false
    unreachable()

proc getBoundBuffer*(target: GLenum): GLuint =
  case target:
  of GlArrayBuffer:
    currentVertexBuffer
  of GlElementArrayBuffer:
    currentElementBuffer
  else:
    doAssert false
    unreachable()

template withBindTexture*(target: GLenum, name: GLuint, body): untyped =
  ## Way to bind buffer object with awareness to currently bound buffer.
  ## It's important as many procedures aren't aware of it, and assume that they *must* bind it,
  ## while quite commonly you have chained commands to a single target.
  assert isInActiveTextureSection
  case target:
  of GlTexture1D:
    if currentTextureBind != name:
      glBindTexture target, name
  of GlTexture2D:
    if currentTextureBind != name:
      glBindTexture target, name
  else:
    doAssert false
    unreachable()

  currentTextureBind = name
  isInTextureBindSection = true
  body
  isInTextureBindSection = false

proc getBoundTexture*(target: GLenum): GLuint = currentTextureBind

template withUseProgram*(name: GLuint, body): untyped =
  assert isInUsedProgramSection

  if name != currentUsedProgram:
    glUseProgram name
    currentUsedProgram = name

  isInUsedProgramSection = true
  body
  isInUsedProgramSection = false

proc getUsedProgram*: GLuint = currentUsedProgram

proc setDepthFunc*(function: GLenum) =
  ## Note: 'withDepthFunc' is preferred over this
  currentDepthFunc = function
  glDepthFunc(function)

template withDepthFunc*(function: GLenum, body) =
  let stash = currentDepthFunc
  if stash != function:
    glDepthFunc function
    currentDepthFunc = function
  body
  if stash != function:
    glDepthFunc stash
    currentDepthFunc = stash

proc setDepthTest*(value: bool) =
  depthTestIsOn = value
  if value: glEnable GlDepthTest
  else: glDisable GlDepthTest

proc setCullFace*(value: bool) =
  cullFaceIsOn = value
  if value: glEnable GlCullFace
  else: glDisable GlCullFace

# todo: We would prefer that Rect2 is assumed to be relative to upper-left, and positive Y pointing downwards
#       Problem is that it would require awareness of current framebuffer dimensions
template withScissor*(rect: Rect2[uint], body): untyped =
  ## Note: Should not be nested.
  ## Coorditate system origin of rect is assumed to be lower-left corner of screen, positive Y is up, positive X is right.
  assert(not isInScissorSection)
  glEnable GlScissorTest
  glScissor rect.origin.x.GLint, rect.origin.y.GLint, rect.extend.width.GLsizei,
      rect.extend.height.GLsizei
  isInScissorSection = true
  body
  glDisable GlScissorTest
  isInScissorSection = false

template withActiveTexture*(unit: TextureUnit, body): untyped =
  assert(not isInActiveTextureSection)
  if currentActiveTextureUnit != unit:
    glActiveTexture(unit)
    currentActiveTextureUnit = unit
  isInActiveTextureSection = true
  body
  isInActiveTextureSection = false

template withMapBuffer*(sym: untyped, target: GLenum, access: GLenum,
    body): untyped =
  assert(not isInMapBufferSection)
  assert(isInVertexBufferBindSection or isInElementBufferBindSection)
  let sym = glMapBuffer(x.target, access)
  isInMapBufferSection = true
  body
  if not glUnmapBuffer(x.target).bool:
    doAssert false, "GL unmap failed"
  isInMapBufferSection = false

template flush*: untyped = glFlush()
