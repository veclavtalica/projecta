import std/[macros, tables]

import pgraphics/gl/gl
import pcommon/io/log
import pcommon/types/[vec]
import pcommon/options
export options

# todo: Way to construct gl shader code that would make nim aware of uniforms, inputs and etc.

type
  Shader* = object
    uniforms: Table[string, UniformVariant]
      # todo: Use SSO available strings for keys, it will be better for both lookup, storage and cache locality
      # todo: We could cache values to check whether sending new data really needed
    program: GLuint

  UniformVariant {.inheritable.} = ref object of RootObj
    ## Uniform variants root that are used for caching of GL uniform state
    ## All possible types are listed here: https://registry.khronos.org/OpenGL-Refpages/gl4/html/glUniform.xhtml
    # todo: Should we use union instead?
    location: GLint

  Uniform1f {.final.} = ref object of UniformVariant
    value: GLfloat

  Uniform1i {.final.} = ref object of UniformVariant
    value: GLint

  Uniform2f {.final.} = ref object of UniformVariant
    value: Vec2[GLfloat]

  Uniform2i {.final.} = ref object of UniformVariant
    value: Vec2[GLint]

  Uniform3f {.final.} = ref object of UniformVariant
    value: Vec3[GLfloat]

  Uniform3i {.final.} = ref object of UniformVariant
    value: Vec3[GLint]

# todo: Uniform4x variants
# todo: Matrix uniform variants

proc `=copy`*(dest: var Shader, source: Shader) {.error.}

proc `=destroy`*(x: var Shader) =
  if x.program != 0:
    glDeleteProgram x.program
    log.message "Shader is freed"
  x.reset()

proc getProgramInfoLog(program: GLuint): Option[string] =
  let logSize = 0.GLint
  glGetProgramiv program, GlInfoLogLength, logSize.unsafeAddr
  if logSize == 0:
    return
  let err = newString logSize
  glGetProgramInfoLog program, logSize, logSize.unsafeAddr, err[0].unsafeAddr
  some err

proc initShader*(vertexSource, fragmentSource: cstring): Option[Shader] =
  ## Compile program from vertex and fragment sources
  ## GL version: 2.0
  proc getCompileError(shader: GLuint): Option[string] =
    let success = 0.GLint
    glGetShaderiv shader, GlCompileStatus, success.unsafeAddr
    if success == GlFalse.GLint:
      let logSize = 0.GLint
      glGetShaderiv shader, GlInfoLogLength, logSize.unsafeAddr
      let err = newString logSize
      glGetShaderInfoLog shader, logSize, logSize.unsafeAddr, err[0].unsafeAddr
      result = some err

  let vertex = glCreateShader(GlVertexShader)
  defer: glDeleteShader(vertex)

  let vertexSourceSize = vertexSource.len.GLsizei
  glShaderSource vertex, 1, cast[cstringArray](vertexSource.unsafeAddr),
      vertexSourceSize.unsafeAddr
  glCompileShader vertex

  let fragment = glCreateShader GlFragmentShader
  defer: glDeleteShader fragment

  let fragmentSourceSize = fragmentSource.len.GLsizei
  glShaderSource fragment, 1, cast[cstringArray](fragmentSource.unsafeAddr),
      fragmentSourceSize.unsafeAddr
  glCompileShader fragment

  let program = glCreateProgram()
  noneDefer: glDeleteProgram(program)

  glAttachShader program, vertex
  glAttachShader program, fragment
  glLinkProgram program

  # Compilation errors are ony meaningful if linking fails
  # https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/WebGL_best_practices#dont_check_shader_compile_status_unless_linking_fails
  let success = 0.GLint
  glGetProgramiv program, GlLinkStatus, success.unsafeAddr
  if success == GlFalse.GLint:
    if (let err = getProgramInfoLog(program); err.isSome):
      log.error err.get()
    if (let err = getCompileError(vertex); err.isSome):
      log.error err.get()
    if (let err = getCompileError(fragment); err.isSome):
      log.error err.get()
    return

  glDetachShader program, vertex
  glDetachShader program, fragment

  some Shader(program: program)

template with*(shader: Shader, body) =
  ## GL version: 2.0
  withUseProgram shader.program:
    body

template generateSetUniform(setter, uniformVariant, basicType: typed): untyped =
  proc setUniform*(shader: var Shader, name: string, value: basicType) =
    assert name.len != 0
    assert getUsedProgram() == shader.program
    shader.uniforms.withValue name, uniformBase:
      var uniform = cast[uniformVariant](uniformBase)
      if uniform.value != value:
        when basicType is Vec3:
          setter uniform.location, value.x, value.y, value.z
        elif basicType is Vec2:
          setter uniform.location, value.x, value.y
        else:
          setter uniform.location, value
        uniform.value = value
    do:
      let location = glGetUniformLocation(shader.program, name[0].addr)
      doAssert location != -1
      when basicType is Vec3:
        setter location, value.x, value.y, value.z
      elif basicType is Vec2:
        setter location, value.x, value.y
      else:
        setter location, value
      shader.uniforms[name] = uniformVariant(location: location, value: value)

proc validate*(shader: Shader) =
  ## Check whether current GL state is well-formed for shader program invocation.
  ## Validation is only executed on debug builds.
  ## Log message is implementation defined.
  when not defined(release):
    glValidateProgram shader.program
    let success = 0.GLint
    glGetProgramiv shader.program, GlValidateStatus, success.unsafeAddr
    if success == GlFalse.GLint:
      if (let msg = getProgramInfoLog(shader.program); msg.isSome):
        log.message msg.get()
      doAssert false

generateSetUniform glUniform1i, Uniform1i, GLint
generateSetUniform glUniform1f, Uniform1f, GLfloat
generateSetUniform glUniform2i, Uniform2i, Vec2[GLint]
generateSetUniform glUniform2f, Uniform2f, Vec2[GLfloat]
generateSetUniform glUniform3i, Uniform3i, Vec3[GLint]
generateSetUniform glUniform3f, Uniform3f, Vec3[GLfloat]
