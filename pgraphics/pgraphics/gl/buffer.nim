##  Generic OpenGL buffer with client-side caching and awareness of capacity on the server.

# https://developer.apple.com/library/archive/documentation/GraphicsImaging/Conceptual/OpenGL-MacProgGuide/opengl_vertexdata/opengl_vertexdata.html

import pgraphics/gl/gl
import pcommon/types/[buffer, vec]
import pcommon/io/log
export buffer

# todo: glMapBuffer interface

type
  # todo: Copy and Read hints
  UsageHint* {.pure, size: sizeof(GLenum).} = enum
    Stream = GlStreamDraw
      ## Modified once, used once
    Static = GlStaticDraw
      ## Modified once, used many times
    Dynamic = GlDynamicDraw
      ## Modified repeatedly, used many times

  BufferTarget* {.pure, size: sizeof(GLenum).} = enum
    ArrayBuffer = GlArrayBuffer
    ElementBuffer = GlElementArrayBuffer

  MappingAccess* {.pure, size: sizeof(GLenum).} = enum
    ReadOnly = GlReadOnly
    WriteOnly = GlWriteOnly
    ReadWriteOnly = GlReadWrite

  # todo: Specialize T to data that could be transferred to GPU
  GlServerBuffer*[T] {.requiresInit.} = object
    name: GLuint
    len: uint            ## T count that is allocated under buffer name.
    usageHint: UsageHint ## Usage hint is enforced to be persistent.
    target: BufferTarget

  GlBuffer*[A: Allocator, T] {.requiresInit.} = object
    serverBuffer: GlServerBuffer[T]
    clientBuffer: Buffer[A, T]
      ## Client side cache which is used for preparing paylads to server.

proc `=copy`*[T](dest: var GlServerBuffer[T], source: GlServerBuffer[T]) {.error.}

proc `=copy`*[A, T](dest: var GlBuffer[A, T], source: GlBuffer[A, T]) {.error.}

proc `=destroy`*[T](x: var GlServerBuffer[T]) =
  if x.name != 0:
    glDeleteBuffers(1, x.name.addr)
    x.name = 0
    log.message "GlServerBuffer is freed"

proc `=destroy`*[A, T](x: var GlBuffer[A, T]) =
  `=destroy`(x.serverBuffer)
  `=destroy`(x.buffer)

proc initGlServerBuffer*(t: typedesc, target: BufferTarget,
    usageHint: UsageHint): GlServerBuffer[t] =
  when t isnot GlPrimitive and (t is AnyVec and t.T isnot GlPrimitive):
    {.fatal: "Invalid type for OpenGL buffer: " & $t.}
  var name {.noInit.}: GLuint
  glGenBuffers 1, name.unsafeAddr
  # todo: Propagate as error?
  #       Actually, is it even possible, gl docs doesnt specify either error or zero return
  doAssert name != 0
  GlServerBuffer[t](name: name, len: 0, usageHint: usageHint, target: target)

# todo: Constructor out of existing buffers
proc initGlBuffer*(allocator: typedesc[Allocator], t: typedesc,
    target: BufferTarget, usageHint: UsageHint): GlBuffer[allocator, t] =
  when t isnot GlPrimitive and (t is AnyVec and t.T isnot GlPrimitive):
    {.fatal: "Invalid type for OpenGL buffer: " & $t.}
  GlBuffer[allocator, t](serverBuffer: initGlServerBuffer(t, target, usageHint),
      buffer: initBuffer(allocator))

template len*(x: GlBuffer): uint = x.clientBuffer.len

# todo: Prevent recursive usages
template with*[T](x: var GlServerBuffer[T], body: untyped): untyped =
  ## All usages of buffer should be in this clause.
  withbindBuffer x.target, x.name:
    body

proc upload*[T](x: var GlServerBuffer[T], m: ReadSpan[T]) =
  ## Upload data to GPU. Reuses the same allocation if it's big enough.
  ## Note: If there's a command issued using given buffer, then it will force execution of it,
  ##       which is suboptimal. Use invalidation if buffers are small enough to be double buffered.
  assert getBoundBuffer(x.target) == x.name
  if m.len > x.len:
    glBufferData(
      x.target, GLsizeiptr sizeof(T).uint * m.len,
      m.asUncheckedArray(), x.usageHint)
    x.len = sizeof(T) * m.len
  else:
    glBufferSubData(x.target, 0, GLsizeiptr sizeof(T).uint * m.len,
      m.asUncheckedArray())

proc doubleBuffer*[T](x: var GlServerBuffer[T]) {.inline.} =
  ## Invalidates buffer, meaning that any future change to it will use new allocation,
  ## while any command that is still issued use of current state is free to own it.
  ## One have to fill new values after that, as old ones will be lost.
  assert getBoundBuffer(x.target) == x.name
  glBufferData(x.target, GLsizeiptr sizeof(T) * x.len, nil, x.usageHint)

proc doubleBuffer*[T](x: var GlServerBuffer[T], newLen: uint) {.inline.} =
  x.len = newLen
  doubleBuffer x

proc upload*[A, T](x: var GlBuffer[A, T]) =
  ## Mirror client side buffer in GPU. Only call it after changes.
  ## Warn: It rebinds current buffer.
  with x.serverBuffer:
    x.serverBuffer.upload(x.clientBuffer.asReadSpan())

proc setLen*[A, T](x: var GlBuffer[A, T], newLen: uint) =
  ## Warn: Only reserves space on client side, without initializing it.
  # x.serverBuffer.setLen(newLen)
  x.clientBuffer.setLen(newLen)

template withMapped*[T](x: var GlServerBuffer[T], sym: untyped,
    access: static MappingAccess, body): untyped =
  withMapBuffer rawMemory, x.target, access:
    let memory = cast[ptr UncheckedArray[T]](rawMemory)
    when access == ReadOnly:
      let sym = initReadSpan(memory, x.len)
    elif access == ReadWrite:
      var sym = initReadWriteSpan(memory, x.len)
    elif access == WriteOnly:
      var sym = initWriteSpan(memory, x.len)
    body

# todo: iterators
