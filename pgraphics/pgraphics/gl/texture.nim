# https://developer.apple.com/library/archive/documentation/GraphicsImaging/Conceptual/OpenGL-MacProgGuide/opengl_texturedata/opengl_texturedata.html

import pgraphics/gl/gl
import pcommon/types/[vec, image]
import pcommon/io/log
export image

# todo: Store allocated storage size under texture name so that we could use sub operation
#       Technically GL drivers could do it already for us, but you never know
# todo: Make texture that owns the client side image
# todo: Use of pixel buffer object

type
  TextureTarget* {.pure, size: sizeof(GLenum).} = enum
    Texture1D = GlTexture1D
    Texture2D = GlTexture2D

  Texture*[C: AnyColor] {.requiresInit.} = object
    ## Handle for GL texture that also stores meta data on client side
    name: GLuint
    target: TextureTarget
    dimensions: Vec2[uint]

proc `=copy`*[C](dest: var Texture[C], source: Texture[C]) {.error.}

proc `=destroy`*[C](x: var Texture[C]) =
  if x.name != 0:
    glDeleteTextures(1, x.name.addr)
    x.name = 0
    log.message "Texture is freed"

func toFormat(format: typedesc[AnyColor]): GLenum =
  when format is ColorRgba: GlRgba
  elif format is ColorBgra: GlBgra
  else: {.fatal: "Unimplemented".}

func toType(format: typedesc[AnyColor]): GLenum =
  when format.T is float: cGlFloat
  else: {.fatal: "Unimplemented".}

# todo: Make target static?
proc initTexture*(format: typedesc[AnyColor], target: TextureTarget): Texture[format] =
  var name {.noInit.}: GLuint
  glGenTextures 1, name.addr
  doAssert name != 0 # todo: Report as error
  Texture[format](name: name, target: target, dimensions: Vec2[uint].default)

# todo: Prevent recursive usages
template with*[A, C](x: Texture[C], unit: TextureUnit, body: untyped): untyped =
  ## All usages of texture should be in this clause.
  withActiveTexture unit:
    withBindTexture x.target, x.name:
      body

# todo: Catch errors
# todo: Check whether GL allows given dimensions
# todo: Allow specification of internal format?
# todo: Allow specification of filtering and wrapping
# todo: Use compressed textures, as bandwidth is more precious than decompression penalty
# todo: Use of GL_UNSIGNED_INT_8_8_8_8_REV
proc upload*[A, C](x: var Texture[C], image: Image[A, C]) =
  ## GL version: 2.0
  assert getBoundTexture(x.target.GLenum) == x.name
  case x.target:
  of Texture1D:
    glTexImage1d(x.target.GLenum, 0,
      GlRgba8.GLint,
      image.dimensions.area.GLsizei, 0,
      C.toFormat, C.toType,
      image.data.asUncheckedArray())
    x.dimensions = (image.dimensions.area, 1.uint)
  of Texture2D:
    glTexImage2d(x.target.GLenum, 0,
      GlRgba8.GLint,
      image.dimensions.width.GLsizei, image.dimensions.height.GLsizei, 0,
      C.toFormat, C.toType,
      image.data.asUncheckedArray())
    x.dimensions = image.dimensions

  glGenerateMipmap x.target.GLenum
  glTexParameteri x.target.GLenum, GlTextureWrapS, GlRepeat
  glTexParameteri x.target.GLenum, GlTextureWrapT, GlRepeat
  glTexParameteri x.target.GLenum, GlTextureMagFilter, GlNearest
  glTexParameteri x.target.GLenum, GlTextureMinFilter, GlNearestMipmapNearest

proc initTexture*[A, C](image: Image[A, C], target: TextureTarget): Texture[C] =
  ## GL version: 2.0
  result = initTexture(C, target)
  result.upload(image)
