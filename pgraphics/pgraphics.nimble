# Package

version = "0.1.0"
author = "Veclav Talica"
description = "projecta[graphics]"
license = "GPL-3.0-only"
srcDir = "./"
skipDirs = @["tests"]

# Dependencies

requires "nim >= 1.6.8"

requires "pcommon", "pprocgen"

requires "opengl"
