
template next*[T](a: ptr T): ptr T =
  cast[ptr T](cast[ptr UncheckedArray[T]](a)[1].addr)

template inc*[T](a: var ptr T) =
  a = cast[ptr T](cast[ptr UncheckedArray[T]](a)[1].addr)

template isAligned*(memory: pointer, t: typedesc): bool =
  cast[uint](memory) mod alignof(t).uint == 0
