import stb_image/read as stbir

import pcommon/types/[color, buffer, vec]
import pcommon/io/log
export color, buffer, vec

# todo: RGB images should still be 4 component for hardware, we might as well make alpha channel uninitialized
#       Alternately this could be done for special HardwareImage type, or directly before uploading

# todo: Hide fields
type
  Image*[A: Allocator, T: AnyColor] {.requiresInit.} = object
    data: Buffer[A, T]
    dimensions: Vec2[uint]

proc `=copy`*[A, T](dest: var Image[A, T], source: Image[A, T]) {.error.}

proc `=destroy`*[A, T](x: var Image[A, T]) =
  `=destroy`(x.data)

template dimensions*(image: Image): auto = image.dimensions

template data*(image: Image): auto = image.data

# todo: Separate loading module from image type definition, as this file implies
# todo: We should be able to just cast the sequence if it's format matches STB
proc load*(allocator: typedesc[Allocator],
           format: typedesc[AnyColor],
           path: string): Image[allocator, format] {.noInit.} =
  var
    channels: int
    width: int
    height: int

  when format.T is float:
    const loadFunction = stbir.loadf
  else:
    {.fatal: "Unimplemented".}

  let data = loadFunction(path, width, height, channels,
      format.numChannels.int)
  doAssert(channels.uint == format.numChannels)
  result = Image[allocator, format](data: initBuffer(allocator, format,
      uint width * height), dimensions: (width.uint, height.uint))
  for i, v in result.data.mpairs:
    v = format.init(data[i * channels.uint], data[i * channels.uint + 1],
        data[i * channels.uint + 2], data[i * channels.uint + 3])
