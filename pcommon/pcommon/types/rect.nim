import pcommon/types/vec
export vec

type
  # todo: Treating them as vectors could be good
  Rect2*[T: SomeNumber] = tuple
    ## Defines rectangle from segment
    origin: Vec2[T]
    extend: Vec2[T]

  Rect3*[T: SomeNumber] = tuple
    ## Defines rectangle from segment
    origin: Vec3[T]
    extend: Vec3[T]

  GRect23*[T: SomeNumber] = Rect2[T] | Rect3[T]

{.push inline.}

template initRect2*[T](p, e: Vec2[T]): Rect2[T] =
  (origin: p, extend: e)

template initRect3*[T](p, e: Vec3[T]): Rect3[T] =
  (origin: p, extend: e)

converter toVec*[T](a: Rect2[T]): Vec2[T] =
  (a.origin.x + a.extend.x, a.origin.y + a.extend.y)

converter toVec*[T](a: Rect3[T]): Vec3[T] =
  (a.origin.x + a.extend.x, a.origin.y + a.extend.y, a.origin.z + a.extend.z)

template x*[T](a: GRect23[T]): T = a.origin.x + a.extend.x
template y*[T](a: GRect23[T]): T = a.origin.y + a.extend.y
template z*[T](a: Rect3[T]): T = a.origin.z + a.extend.z

# todo: Implement over macro iterator for coordinates of generic rect
func fit*[T](a: Rect2[T], extend: Vec2[T]): Rect2[T] =
  result = a
  assert a.extend < extend
  if a.origin.x < 0:
    result.origin.x = 0
  elif a.origin.x + a.extend.x >= extend.x:
    result.origin.x -= a.origin.x + a.extend.x - extend.x
  if a.origin.y < 0:
    result.origin.y = 0
  elif a.origin.y + a.extend.y >= extend.y:
    result.origin.y -= a.origin.y + a.extend.y - extend.y

template contains*[T](a: Vec2[T], item: Rect2[T]): bool =
  item.toVec < a

template contains*[T](a: Vec3[T], item: Rect3[T]): bool =
  item.toVec < a

{.pop.}
