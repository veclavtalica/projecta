import pcommon/io/log
import pcommon/types/[allocator, span, integers]
export allocator, span

import pcommon/private/annotations

# todo: Alignment specifications and ways to safery check.
# todo: Make sure destructors are called everywhere on clearing items.
# todo: Use uint everywhere instead of 'Positive'
# todo: Offset optimization.

const
  DefaultCapacity = 16
  CapacityGrowth = 8

type
  Buffer*[A: Allocator, T] = object
    allocation: Allocation[A, T]
    len: uint ## Initialized values

{.push inline.}

func initBuffer*[I, T](arr: ptr array[I, T],
                       len: uint = I.high + 1): Buffer[StaticAllocator, T] {.clean.} =
  ## Warn: Given pointer should outlive created buffer.
  Buffer[StaticAllocator, T](
    allocation: initAllocation(StaticAllocator, arr),
    len: len)

proc initBuffer*(allocator: typedesc[Allocator],
                 t: typedesc,
                 capacity = DefaultCapacity.uint): Buffer[allocator, t] {.clean.} =
  Buffer[allocator, t](
    allocation: if capacity != 0: initAllocation(allocator, t,
        capacity) else: Allocation[allocator, t].default,
    len: 0)

template len*(buffer: Buffer): uint {.clean.} = buffer.len

template capacity*(buffer: Buffer): uint {.clean.} = buffer.allocation.len

template freeSpace*(buffer: Buffer): uint {.clean.} = buffer.capacity - buffer.len

template allocationAddress*(buffer: Buffer): uint {.clean.} = cast[uint](
    buffer.allocation.uintAddress)
  ## Should be used cautiously!

func asReadSpan*(buffer: Buffer): auto {.clean.} =
  ## Warn: AnyReadSpan is only valid until resizing or freeing
  initReadSpan(buffer.allocation.asUncheckedArray(), buffer.len)

func asReadWriteSpan*(buffer: Buffer): auto {.clean.} =
  ## Warn: AnyReadSpan is only valid until resizing or freeing
  initReadWriteSpan(buffer.allocation.asUncheckedArray(), buffer.len)

func asWriteSpan*(buffer: Buffer): auto {.clean.} =
  ## Warn: AnyReadSpan is only valid until resizing or freeing
  initWriteSpan(buffer.allocation.asUncheckedArray(), buffer.len)

func asUncheckedArray*(buffer: Buffer): auto {.clean.} =
  ## Warn: Beware of out of bounds access, use spans unless you check the size
  buffer.allocation.asUncheckedArray()

# todo: Specify range by HSlice
# todo: Specialize for cases where start is statically known
func asArrayPtr*(buffer: Buffer, start: uint,
    extend: static uint): auto {.clean.} =
  assert buffer.capacity >= start.uint + extend.uint
  assert buffer.len >= start.uint + extend.uint
  cast[ptr array[extend, buffer.T]](buffer.allocation.asUncheckedArray()[start].addr)

func asArrayPtrOfType*(buffer: Buffer, t: typedesc, start: uint,
                       extend: static uint): auto {.clean.} =
  ## Cast range of buffer to ptr array of other type.
  ## Start should be of appropriate alignment, and extend should fit given type whole.
  # todo: Forbid this for any buffer.T besides byte/uint8? As aliasing rules might make it UB.
  # todo: Could this assert be static?
  assert((buffer.allocationAddress mod alignof(t).uint) == 0)
  static: assert(((sizeof(buffer.T).uint * extend.uint) mod sizeof(t).uint) == 0)
  const extendOfType = (sizeof(buffer.T).uint * extend.uint) div sizeof(t).uint
  cast[ptr array[extendOfType, t]](buffer.asArrayPtr(start, extend))

func `[]`*[A, T](buffer: Buffer[A, T], idx: uint): lent T {.clean.} =
  ## Note: This returns copy.
  assert buffer.capacity > idx
  assert buffer.len > idx
  buffer.allocation.asUncheckedArray()[idx]

proc `[]=`*[A, T](buffer: Buffer[A, T], idx: uint, v: sink T) {.clean.} =
  assert buffer.capacity > idx
  assert buffer.len > idx
  buffer.allocation.asUncheckedArray()[idx] = v

# todo: It would be better to have 'var buffer.T' return, but Nim cannot reason about it as of yet.
func atPtr*(buffer: Buffer, idx: uint): ptr buffer.T {.clean.} =
  assert buffer.capacity > idx
  assert buffer.len > idx
  buffer.allocation.asUncheckedArray()[idx].addr

# todo: Should it return nil on empty?
func lastPtr*(buffer: Buffer): ptr buffer.T {.clean.} =
  assert buffer.len != 0
  buffer.allocation.asUncheckedArray()[buffer.len - 1].addr

proc ensureCapacity*(buffer: var Buffer, capacity: positive uint) {.clean.} =
  if buffer.capacity < capacity.uint:
    buffer.allocation.resize capacity

# todo: Rename to something more verbose and clear in intent
proc forceLen*(buffer: var Buffer, newLen: uint) {.clean.} =
  ## Sets len and allocates new space if needed, or truncating without destructing existing items
  ## Note that no newly allocated item is initialized
  buffer.ensureCapacity newLen
  buffer.len = newLen

proc setLen*(buffer: var Buffer, newLen: uint) {.clean.} =
  ## Sets len and allocates new space if needed, or truncated and destructs existing items
  ## Note that no newly allocated item is initialized
  if buffer.len > newLen:
    when compiles(`=destroy`(x.memory[0])):
      assert(x.len < x.capacity)
      for i in newLen..<buffer.len:
        `=destroy`(i.atPtr(i)[])
  else:
    buffer.ensureCapacity(newLen)
  buffer.len = newLen

# todo: Current semantics assume that allocation is never nil.
#       We could free it fully to then reallocate, but it requires patching.
proc shrink*(buffer: var Buffer) {.clean.} =
  ## Shrinks allocation to the len of buffer
  if buffer.capacity != buffer.len:
    buffer.allocation.resize(if buffer.len !=
        0: buffer.len else: DefaultCapacity)

# todo: Fine-tune the growth to minimize allocation count.
proc add*(buffer: var Buffer, v: sink buffer.T) {.clean.} =
  ## Push item at the back, reallocating if needed
  assert buffer.capacity >= buffer.len
  if buffer.capacity == buffer.len:
    buffer.allocation.resize(buffer.capacity + CapacityGrowth)
  buffer.allocation.asUncheckedArray()[buffer.len] = v
  buffer.len.inc

proc pop*(buffer: var Buffer): buffer.T {.clean.} =
  ## Pop last element
  assert buffer.len != 0
  buffer.len.dec
  result = buffer.allocation.asUncheckedArray()[buffer.len]

proc stealAllocation*(buffer: var Buffer): auto {.clean, noInit.} =
  ## Steal managed allocation from inside buffer, making it invalid from now on.
  assert buffer.allocation.asUncheckedArray != nil
  buffer.allocation.move()

converter toReadSpan*(buffer: Buffer): auto = buffer.asReadSpan()

converter toReadWriteSpan*(buffer: var Buffer): auto = buffer.asReadWriteSpan()

iterator items*(buffer: Buffer): lent buffer.T {.clean.} =
  for v in buffer.asReadSpan():
    yield v

iterator mitems*(buffer {.noalias.}: var Buffer): var buffer.T {.clean.} =
  for v in buffer.asReadWriteSpan().mitems:
    yield v

# todo: Template for index type
# todo: Could it be that having only one incrementing variable is significantly better?
#       We could implement iterators via macro, where one of index or variable pointer is inferred from another.
iterator mpairs*(buffer {.noalias.}: var Buffer): tuple[idx: uint,
    v: var buffer.T] {.clean.} =
  var i = 0.uint
  for v in buffer.mitems:
    yield (i, v)
    i.inc

proc `=copy`*[A, T](dest: var Buffer[A, T], source: Buffer[A, T]) {.error.}

proc `=destroy`*[A, T](x: var Buffer[A, T]) {.clean.} =
  if x.len != 0:
    log.message "Buffer is freed: ", cast[uint](x.allocation.asUncheckedArray())
    when compiles(`=destroy`(x.memory[0])):
      assert(x.len < x.capacity)
      for v in x.mitems:
        `=destroy`(v)
    x.len = 0
  `=destroy`(x.allocation)

{.pop.}
