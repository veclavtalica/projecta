import pcommon/io/log
import pcommon/types/[span, integers]
export span

import pcommon/private/annotations

# todo: What's about memory error propagations? Currently NimAllocator raises defects which are uncatchable
# todo: StaticAllocation without dynamic allocation and freeing, needed for static buffer
# todo: Way to reuse allocation by swapping their types
# todo: Allow external allocators
# todo: Cast as not having side effects
# todo: Generic memory checker over allocators
# todo: Ability to satisfy alignment requirements, which is especially crucial for vector arrays and SIMD

type
  StaticAllocator* = object
    ## Virtual allocator bound to unmanaged memory.

  NimAllocator* = object
    ## Thread-local heap allocator using std/system

  NimGlobalAllocator* = object
    ## Global heap allocator using std/system

  Allocator* = StaticAllocator | NimAllocator | NimGlobalAllocator

  Allocation*[A: Allocator, T] = object
    memory: ptr UncheckedArray[T]
    len: uint

{.push inline.}

template len*(allocation: Allocation): uint = allocation.len

# todo: We need this currently pretty much only for alignment checks,
#       It would be better to instead provide ways to check alignment or padding here,
#       instead of exposing raw address
template uintAddress*(allocation: Allocation): uint {.clean.} =
  ## Warning: Should be used cautiously!
  cast[uint](allocation.memory)

func asReadSpan*(allocation: Allocation): auto {.clean.} =
  ## Warning: AnyReadSpan is only valid until resizing or freeing
  initReadSpan(allocation.memory, allocation.len)

func asReadWriteSpan*(allocation: Allocation): auto {.clean.} =
  ## Warning: AnyReadSpan is only valid until resizing or freeing
  initReadWriteSpan(allocation.memory, allocation.len)

func asUncheckedArray*(allocation: Allocation): auto {.clean.} =
  ## Warn: Beware of out of bounds access, use spans unless you check the size
  allocation.memory

func `[]`*[A, T](allocation: Allocation[A, T], idx: uint): lent T {.clean.} =
  assert allocation.len > idx.uint
  allocation.asUncheckedArray()[idx]

func `[]`*[A, T](allocation: var Allocation[A, T], idx: uint): var T {.clean.} =
  assert allocation.len > idx.uint
  allocation.asUncheckedArray()[idx]

proc `[]=`*[A, T](allocation: Allocation[A, T], idx: uint,
    v: sink T) {.clean.} =
  assert allocation.len > idx.uint
  allocation.asUncheckedArray()[idx] = v

proc createImpl(allocator: typedesc[NimAllocator],
                t: typedesc,
                size: positive uint,
                doZero = false): ptr UncheckedArray[t] {.clean.} =
  cast[ptr UncheckedArray[t]](if doZero: create(t, size.Positive) else: createU(
      t, size.Positive))

proc createImpl(allocator: typedesc[NimGlobalAllocator],
                t: typedesc,
                size: positive uint,
                doZero = false): ptr UncheckedArray[t] {.clean.} =
  cast[ptr UncheckedArray[t]](if doZero: createShared(t,
      size.Positive) else: createSharedU(t, size.Positive))

func resizeImpl[A: StaticAllocator, T](allocation: var Allocation[A, T],
    size: positive uint) {.clean.} =
  ## Does nothing but checks whether it can accommodate requested size.
  doAssert(allocation.len >= size.uint)

# todo: Reassigning resize might be better as it would invalidate borrowed views from it
proc resizeImpl[A: NimAllocator, T](allocation: var Allocation[A, T],
    size: positive uint) {.clean.} =
  allocation.memory = cast[ptr UncheckedArray[T]](resize(allocation.memory[
      0].addr, size.Positive))
  allocation.len = size

proc resizeImpl[A: NimGlobalAllocator, T](allocation: var Allocation[A, T],
    size: positive uint) {.clean.} =
  allocation.memory = cast[ptr UncheckedArray[T]](resizeShared(
      allocation.memory[0].addr, size.Positive))
  allocation.len = size

proc freeImpl[A: StaticAllocator, T](allocation: var Allocation[A,
    T]) {.clean.} =
  assert(allocation.memory != nil)

proc freeImpl[A: NimAllocator, T](allocation: var Allocation[A, T]) {.clean.} =
  assert(allocation.memory != nil)
  dealloc(allocation.memory[0].addr)

proc freeImpl[A: NimGlobalAllocator, T](allocation: var Allocation[A,
    T]) {.clean.} =
  doAssert(allocation.memory != nil)
  deallocShared(allocation.memory[0].addr)

proc initAllocation*[I, T](allocator: typedesc[StaticAllocator],
                     arr: ptr array[I, T],
                     doZero = false): Allocation[allocator, T] =
  if doZero:
    # todo: Could arr.reset work?
    zeroMem(arr, arr[].len * sizeof T)
  Allocation[allocator, T](
    memory: cast[ptr UncheckedArray[T]](arr),
    len: arr[].len.uint)

# todo: Making doZero comptime might be good.
proc initAllocation*(allocator: typedesc[Allocator],
                     t: typedesc,
                     size: positive uint,
                     doZero = false): Allocation[allocator, t] =
  Allocation[allocator, t](
    memory: createImpl(allocator, t, size, doZero),
    len: size.uint)

proc resizeZero*(allocation: var Allocation, size: positive uint) {.clean.} =
  ## Generic resizing with zeroing
  let prevSize = allocation.len
  allocation.resizeImpl(size)
  if size.uint > prevSize:
    zeroMem(allocation.memory[prevSize].addr, sizeof(allocation.T).uint * (
        size.uint - prevSize))

proc resize*(allocation: var Allocation, size: positive uint) {.clean.} =
  ## Binds generic resize to particular allocator
  allocation.resizeImpl(size)

proc `=copy`*[A, T](dest: var Allocation[A, T], source: Allocation[A, T]) {.error.}

proc `=destroy`*[A, T](allocation: var Allocation[A, T]) {.clean.} =
  if not allocation.memory.isNil:
    log.message "Allocator is freed: ", cast[uint](allocation.memory)
    allocation.freeImpl()
    allocation.memory = nil
    allocation.len = 0

{.pop.}
