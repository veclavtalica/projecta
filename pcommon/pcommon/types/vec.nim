import std/math

import pcommon/private/annotations

# todo: Implement it over cglm for types that are supported there
# todo: According to vmath different type representation behave differently,
#       but they haven't tested tuples there. It might be suboptimal, we need to check.
# todo: Align Vec3 to Vec4 requirements?
#       https://www.researchgate.net/profile/Alexandre-Eichenberger/publication/220752110_Vectorization_for_SIMD_Architectures_with_alignment_constraints/links/0912f512672cb2a4ef000000/Vectorization-for-SIMD-Architectures-with-alignment-constraints.pdf
# todo: Could we apply vector attribute of gcc and clang to it?
type
  Vec2*[T: SomeNumber] {.bycopy.} = tuple[x, y: T]
    ## Two dimensional vector with assumed origin of (0, 0)
  Vec3*[T: SomeNumber] = tuple[x, y, z: T]
    # todo: Align for easier automatic vectorization
  Vec4*[T: SomeNumber] = tuple[x, y, z, w: T]

  AnyVec*[T] = Vec2[T] | Vec3[T] | Vec4[T]

{.push inline.}

template x*[T](v: AnyVec[T]): T = v[0]
template y*[T](v: AnyVec[T]): T = v[1]
template z*[T](v: Vec3[T]): T = v[2]

template `x=`*[T](v: var AnyVec[T], s: sink T) = v[0] = s
template `y=`*[T](v: var AnyVec[T], s: sink T) = v[1] = s
template `z=`*[T](v: var Vec3[T], s: sink T) = v[2] = s

template width*[T](v: AnyVec[T]): T = v.x
template height*[T](v: AnyVec[T]): T = v.y
template depth*[T](v: Vec3[T]): T = v.z

template `width=`*[T](v: var AnyVec[T], s: sink T) = v.x = s
template `height=`*[T](v: var AnyVec[T], s: sink T) = v.y = s
template `depth=`*[T](v: var Vec3[T], s: sink T) = v.z = s

template initVec2*[T](x: T): auto = (x, x)
template initVec2*[T](x, y: T): auto = (x, y)
template initVec3*[T](x: T): auto = (x, x, x)
template initVec3*[T](x, y, z: T): auto = (x, y, z)
template initVec4*[T](x: T): auto = (x, x, x, x)
template initVec4*[T](x, y, z, w: T): auto = (x, y, z, w)

template unit*(t: typedesc[Vec2]): t = (t.T(1), t.T(1))
template unit*(t: typedesc[Vec3]): t = (t.T(1), t.T(1), t.T(1))

template `xy=`*[T](a: var AnyVec[T], b: Vec2[T]) =
  a.x = b.x
  a.y = b.y

template xy*[T](a: AnyVec[T]): Vec2[T] = (a.x, a.y)

template convertTo*[T](a: Vec2[T], t: typedesc[SomeNumber]): Vec2[t] = (a.x.t, a.y.t)
template convertTo*[T](a: Vec3[T], t: typedesc[SomeNumber]): Vec3[t] = (a.x.t,
    a.y.t, a.z.t)

template castTo*[T](a: Vec2[T], t: typedesc[SomeNumber]): Vec2[t] = (cast[t](
    a.x), cast[t](a.y))
template castTo*[T](a: Vec3[T], t: typedesc[SomeNumber]): Vec3[t] = (cast[t](
    a.x), cast[t](a.y), cast[t](a.z))

template genOp(op: untyped) =
  template op*[T](a, b: Vec2[T]): auto = (op(a.x, b.x), op(a.y, b.y))
  template op*[T](a, b: Vec3[T]): auto = (op(a.x, b.x), op(a.y, b.y), op(a.z, b.z))
  template op*[T](a: Vec2[T], s: T): auto = (op(a.x, s), op(a.y, s))
  template op*[T](a: Vec3[T], s: T): auto = (op(a.x, s), op(a.y, s), op(a.z, s))

genOp(`+`)
genOp(`-`)
genOp(`*`)
genOp(`/`)
genOp(`mod`)
genOp(`div`)

template genOpAnd(op: untyped) =
  template op*[T](a, b: Vec2[T]): auto = op(a.x, b.x) and op(a.y, b.y)
  template op*[T](a, b: Vec3[T]): auto = op(a.x, b.x) and op(a.y, b.y) and op(a.z, b.z)
  template op*[T](a: Vec2[T], s: T): auto = op(a.x, s) and op(a.y, s)
  template op*[T](a: Vec3[T], s: T): auto = op(a.x, s) and op(a.y, s) and op(a.z, s)

genOpAnd(`==`)
genOpAnd(`!=`)
genOpAnd(`>=`)
genOpAnd(`<=`)
genOpAnd(`>`)
genOpAnd(`<`)

# todo: Rename to 'volume' instead?
template area*[T](v: Vec2[T]): T =
  when T is SomeUnsignedInt:
    v.x * v.y
  else:
    abs (v.x * v.y)

template area*[T](v: Vec3[T]): T =
  when T is SomeUnsignedInt:
    v.x * v.y * v.z
  else:
    abs (v.x * v.y * v.z)

# todo: Why is it like that, lol
template contains*[T](a, item: Vec2[T]): bool =
  if a >= 0 and item >= 0:
    item.x < a.x and item.y < a.y
  else:
    # todo: We might consider implementing areas on negative ranges, but its a bit trickier
    false

iterator items*[T: SomeUnsignedInt](a: Vec2[T]): Vec2[T] {.clean.} =
  for y in 0.T..<a.height:
    for x in 0.T..<a.width:
      yield (x, y)

iterator coords*[T: SomeUnsignedInt](a: Vec2[T]): tuple[idx: uint, coords: Vec2[T]] {.clean.} =
  var i = 0.uint
  for pos in a:
    yield (i, pos)
    i.inc

iterator coords*[T: SomeUnsignedInt](a: Vec3[T]): tuple[idx: uint, coords: Vec3[T]] {.clean.} =
  var i = 0.uint
  for z in 0.T..<a.depth:
    for y in 0.T..<a.height:
      for x in 0.T..<a.width:
        yield (i, (x, y, z))
      i.inc

{.pop.}
