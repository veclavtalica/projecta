import pcommon/types/vec
export vec

# todo: use Use span instead of array?
type
  Matrix2*[C: static Vec2[uint], T] = object
    ## Defines 2 dimensional matrix interface over linear row-major array
    cells: array[C.area, T]

  Matrix3*[C: static Vec3[uint], T] = object
    ## Defines 3 dimensional matrix interface over linear row-major array
    cells: array[C.area, T]

  GMatrix23*[C, T] = Matrix2 | Matrix3

{.push inline.}

template initMatrix2*[T](dims: static Vec2[uint], cells: array[dims.area,
    T]): Matrix2[dims, T] =
  Matrix2(cells: cells)

template initMatrix3*[T](dims: static Vec3[uint], cells: array[dims.area,
    T]): Matrix3[dims, T] =
  Matrix3(cells: cells)

template toIdx*[T](p, dims: Vec2[T]) = p.x + p.y * dims.width
template toIdx*[T](p, dims: Vec3[T]) = p.x + p.y * dims.width + p.z * dims.height

template `[]`*[C, T](a: Matrix2[C, T], p: Vec2[uint]): T =
  assert p in C
  a.cells[toIdx(p, C)]

template `[]`*[C, T](a: Matrix2[C, T], idx: uint): T =
  assert idx < C.area
  a.cells[idx]

template `[]=`*[C, T](a: var Matrix2[C, T], p: Vec2[uint], v: sink T) =
  assert p in C
  a.cells[toIdx(p, C)] = v

# todo: Implement over generic matrix
template `[]`*[C, T](a: Matrix3[C, T], p: Vec3[uint]): T =
  assert p in C
  a.cells[toIdx(p, C)]

template `[]`*[C, T](a: Matrix3[C, T], idx: uint): T =
  assert idx < C.area
  a.cells[idx]

template `[]=`*[C, T](a: var Matrix3[C, T], p: Vec3[uint], v: sink T) =
  assert p in C
  a.cells[toIdx(p, C)] = v

{.pop.}
