
template positive*(t: typedesc[SomeNumber]): auto = range[1.t .. t.high]
