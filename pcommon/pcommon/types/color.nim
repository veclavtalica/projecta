import pcommon/types/vec
export vec

type
  ColorRgba*[T: SomeNumber] = distinct Vec4[T]
    ## Normalized

  ColorBgra*[T: SomeNumber] = distinct Vec4[T]
    ## Normalized, usually preferred by GPUs

  Any4ComponentColor[T] = ColorRgba[T] | ColorBgra[T]

  AnyColor*[T] = Any4ComponentColor[T]

# todo: Template that:

{.push inline.}

converter toVec[T](x: Any4ComponentColor[T]): Vec4[T] = Vec4[T](x)
converter toVec[T](x: var Any4ComponentColor[T]): var Vec4[T] = Vec4[T](x)

func red*(x: ColorRgba): x.T = x.toVec[0]
func green*(x: ColorRgba): x.T = x.toVec[1]
func blue*(x: ColorRgba): x.T = x.toVec[2]
func alpha*(x: ColorRgba): x.T = x.toVec[3]

proc `red=`*(x: var ColorRgba, v: sink x.T) = x.toVec[0] = v
proc `green=`*(x: var ColorRgba, v: sink x.T) = x.toVec[1] = v
proc `blue=`*(x: var ColorRgba, v: sink x.T) = x.toVec[2] = v
proc `alpha=`*(x: var ColorRgba, v: sink x.T) = x.toVec[3] = v

func blue*(x: ColorBgra): x.T = x.toVec[0]
func green*(x: ColorBgra): x.T = x.toVec[1]
func red*(x: ColorBgra): x.T = x.toVec[2]
func alpha*(x: ColorBgra): x.T = x.toVec[3]

proc `blue=`*(x: var ColorBgra, v: sink x.T) = x.toVec[0] = v
proc `green=`*(x: var ColorBgra, v: sink x.T) = x.toVec[1] = v
proc `red=`*(x: var ColorBgra, v: sink x.T) = x.toVec[2] = v
proc `alpha=`*(x: var ColorBgra, v: sink x.T) = x.toVec[3] = v

template numChannels*[T](x: typedesc[Any4ComponentColor[T]]): uint = 4

func init*[T](t: typedesc[Any4ComponentColor[T]], red, green, blue,
    alpha: sink T): t {.noInit.} =
  result.red = red
  result.green = green
  result.blue = blue
  result.alpha = alpha

func `==`*[T](a, b: Any4ComponentColor[T]): bool =
  when a is typeof(b):
    a.toVec == b.toVec
  else:
    a.red == b.red and a.green == b.green and a.blue == b.blue and a.alpha == b.alpha

{.pop.}
