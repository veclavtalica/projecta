## Utilities for working over memory unmanaged by nim
## 
## Rational:
## Sometimes there are cases where Nim cannot reason about lifetime of view, so, we bypass it.
## Const typing enforced at compile time - in comparison to openArray which are always open to mutation.
## It also allows for access control of spans as a field of bigger object, no matter its mutability.

# todo: Static Spans that dont require storing len dynamically?
# todo: We could get rid of need to subtract 1 to get highest indexable if we store
#       last index instead of len, and infer empty sequence from nil memory

import pcommon/ptrutils

import pcommon/private/annotations

type
  SpanImpl[T] {.requiresInit.} = object
    memory: ptr UncheckedArray[T]
    len: uint

  ReadSpan*[T] = distinct SpanImpl[T]

  ReadWriteSpan*[T] = distinct SpanImpl[T]

  WriteSpan*[T] = distinct SpanImpl[T]
    ## Is useful when semantics require only ability to write to memory.
    ## This happens when uninitialized memory is given,
    ## or its not guaranteed that reading will give well-formed results.

  AnyReadSpan[T] = ReadSpan[T] | ReadWriteSpan[T]

  AnyWriteSpan[T] = ReadWriteSpan[T] | WriteSpan[T]

  AnySpan[T] = ReadSpan[T] | ReadWriteSpan[T] | WriteSpan[T]

{.push inline.}

func initReadSpan*[T](memory: ptr UncheckedArray[T],
    len: uint): auto {.clean.} =
  assert isAligned(memory, T)
  cast[ReadSpan[T]](SpanImpl[T](memory: memory, len: len))

func initReadWriteSpan*[T](memory: ptr UncheckedArray[T],
    len: uint): ReadWriteSpan[T] =
  assert isAligned(memory, T)
  cast[ReadWriteSpan[T]](SpanImpl[T](memory: memory, len: len))

func initWriteSpan*[T](memory: ptr UncheckedArray[T],
    len: uint): auto {.clean.} =
  assert isAligned(memory, T)
  cast[WriteSpan[T]](SpanImpl[T](memory: memory, len: len))

converter toImpl(span: AnySpan): auto = cast[SpanImpl[span.T]](span)

template len*(span: AnySpan): uint =
  mixin toImpl
  span.toImpl.len

converter toReadSpan*(span: ReadWriteSpan): auto {.clean.} = cast[ReadSpan[
    span.T]](span)

converter toWriteSpan*(span: ReadWriteSpan): auto {.clean.} = cast[WriteSpan[
    span.T]](span)

func `[]`*(span: ReadSpan, idx: uint): lent span.T {.clean.} =
  assert(idx < span.toImpl.len)
  span.toImpl.memory[idx]

func `[]`*(span: AnyWriteSpan, idx: uint): var span.T {.clean.} =
  assert(idx < span.toImpl.len)
  span.toImpl.memory[idx]

proc `[]=`*(span: AnyWriteSpan, idx: uint, v: sink span.T) {.clean.} =
  assert(idx < span.toImpl.len)
  span.toImpl.memory[idx] = v

func asUncheckedArray*(span: AnySpan): ptr UncheckedArray[
    span.T] {.clean.} =
  ## Warning: Access rights should be preserved, otherwise behavior is undefined.
  span.toImpl.memory

func asArrayPtr*(span: AnySpan, start: uint,
    extend: static uint): auto {.clean.} =
  ## Warning: Access rights should be preserved, otherwise behavior is undefined.
  assert span.len >= start.uint + extend.uint
  cast[ptr array[extend, span.T]](span.asUncheckedArray()[start].addr)

iterator items*(span: AnyReadSpan): lent span.T {.clean.} =
  for i in 0..<span.len:
    yield span.toImpl.memory[i]

iterator mitems*(span: AnyWriteSpan): var span.T {.clean.} =
  for i in 0..<span.len:
    yield span.toImpl.memory[i]

{.pop.}
