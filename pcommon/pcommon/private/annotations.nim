
template clean*(f: untyped): untyped =
  {.push gcsafe, raises: [].}
  f
  {.pop.}
