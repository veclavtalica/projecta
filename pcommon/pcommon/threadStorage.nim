## Interface for management of temporary thread local buffers
import std/macros

import pcommon/types/buffer
import pcommon/io/log
export buffer

# todo: Current implementation is hackish as hell.

# todo: Investigate zeroing strategies, as it's important for reusable non-persistent buffers:
#       https://citeseerx.ist.psu.edu/document?repid=rep1&type=pdf&doi=42524667961442587a9eac9b6612d8eb7690f0e6

type
  Entry = object
    index, start: uint

var buffers {.threadvar.}: Buffer[NimAllocator, Buffer[NimAllocator, uint8]]
var stack {.threadvar.}: Buffer[NimAllocator, Entry]
  # todo: Make FIFO type for this

# todo: Make it inlinable with minimal stack pollution.
# todo: Test whether alignment works on type mixing.
proc findStorage(t: typedesc, size: static uint, doZero: static bool = false): ptr array[size,
    t] {.gcsafe, raises: [], noInit.} =
  when doZero:
    defer:
      zeroMem result, size

  const size = sizeof(t).uint * size

  for i, buffer in buffers.mpairs:
    when alignof(t) != 1:
      let padding = (alignof(t).uint - 1) - (
          buffer.allocationAddress mod alignof(t).uint)
    else:
      const padding = 0.uint

    if buffer.len == 0 or buffer.freeSpace >= size + padding:
      # If given allocation has enough capacity, no matter used or not - use its memory for new storage.
      stack.add Entry(index: i, start: buffer.len)
      buffer.forceLen(buffer.len + size + padding)
      return buffer.asArrayPtrOfType(t, buffer.len - size, size)

  # Create new allocation if cannot reuse any.
  stack.add Entry(index: buffers.len, start: 0)
  var buffer = initBuffer(NimAllocator, uint8, size)
  buffer.forceLen size # Mark as used
  # todo: Add padding if this happens
  doAssert buffer.allocationAddress mod alignof(t).uint == 0
  buffers.add buffer
  return buffers.atPtr(buffers.len - 1)[].asArrayPtrOfType(t, 0, size)

# todo: Comptime error on moving storage
template binding(sym, symInit, body): untyped =
  block:
    var sym = symInit
    body
    # Cleaning relies on order of creation, stack top is always popped.
    # For that we must ensure that nothing will live longer than spot it was init'ed at.
    let entry = stack.pop()
    buffers.atPtr(entry.index)[].forceLen(entry.start)

# todo: Find a way to do it without nesting
template withThreadBuffer*(sym: untyped, t: typedesc,
                           size: uint = 1,
                           body): untyped =
  ## Binds thread local storage to given symbol from shared memory pool.
  ## Warn: Dont pass 'sym' to anywhere outside of current block, as lifetime of it is bound to block.
  binding(sym, initBuffer(findStorage(t, size), 0), body)

template withThreadBufferZero*(sym: untyped, t: typedesc,
                              size: uint = 1,
                              body): untyped =
  ## Binds thread local storage to given symbol from shared memory pool, while also zeroing it.
  ## Warn: Dont pass 'sym' to anywhere outside of current block, as lifetime of it is bound to block.
  binding(sym, initBuffer(findStorage(t, size, doZero = true), 0), body)

# todo: Require initialization, so that destructor could be called here too
template withThreadObject*(sym: untyped, t: typedesc, body): untyped =
  ## Binds thread local storage to given symbol from shared memory pool.
  ## Warn: Dont pass 'sym' to anywhere outside of current block, as lifetime of it is bound to block.
  ## Warn: Destructors are not currently called after end of scope.
  binding(sym, cast[ptr t](findStorage(t, 1)), body)

template withThreadObjectZero*(sym: untyped, t: typedesc, body): untyped =
  ## Binds thread local storage to given symbol from shared memory pool.
  ## Warn: Dont pass 'sym' to anywhere outside of current block, as lifetime of it is bound to block.
  ## Warn: Destructors are not currently called after end of scope.
  binding(sym, cast[ptr t](findStorage(t, 1, doZero = true)), body)

proc shrinkThreadStorage* {.gcsafe, raises: [].} =
  ## Minimize used space by thread storage, if something is not used - it will be fully freed.
  stack.shrink()
  for i in 0..<buffers.len:
    # We assume that empty allocations will not be followed by non-empty ones.
    if buffers[i].len == 0:
      buffers.forceLen(i)
      buffers.shrink()
      break
