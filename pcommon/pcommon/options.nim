import std/options
export options

# todo: Test it all
# todo: Distinct ErrorOr[T] where some is error payload, and none is success

template noneDefer*(body: untyped): untyped =
  ## Execute block if function returns none variant
  ## Requires to be substituted in function that returns Option[T]
  defer:
    if result.isNone:
      body

template someDefer*(body: untyped): untyped =
  ## Execute block if function returns some variant
  ## Requires to be substituted in function that returns Option[T]
  defer:
    if result.isSome:
      body

template must*(body: untyped): untyped =
  ## Ensures that expression that resolves to option has value, otherwise program quits
  var tmp = body
  if unlikely(tmp.isNone()):
    quit(1)
  tmp.get().move()

# todo: 'tmp' should not clash with other symbols
template unwrap*(body: untyped): untyped {.dirty.} =
  ## Evaluate to value from option, or return none from function it is called in
  var tmp = body
  if unlikely(tmp.isNone()):
    # todo: Potentially introduces overhead, we might somehow infer
    #       whether result could be assigned at this point or not
    result.reset() # Makes it none if something was already assigned to
    return
  tmp.get().move()
