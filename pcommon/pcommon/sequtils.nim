## Utilities over Nim seq and array types

# todo: Is there a way to not use intermediate allocation?
func shrink*[T](a: var seq[T]) {.raises: [].} =
  ## Shrinks memory in place of sequence to just fit what's required.
  var tmp = newSeqOfCap[T](a.len)
  tmp[0..^1] = a.toOpenArray(0, a.len)
  a = tmp

# todo: Make it work comptime
func initArrayOf*[I, T](init: T): array[I, T] =
  for v in result.mitems:
    v = init
