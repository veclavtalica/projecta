## Compiler intrinsics bridge.

# todo: Emulate intrinsics if they're not available

# https://gcc.gnu.org/onlinedocs/gcc/Other-Builtins.html#Other-Builtins

when defined(clang) or defined(gcc) or defined(llvm_gcc):
  const hasOverflowArithmetic* = true
  type OverflowArithmeticTypes* = cint|clong|clonglong|cuint|culong|culonglong
  func addOverflow*[T: OverflowArithmeticTypes](a, b: T,
      result: ptr T): bool {.importc: "__builtin_add_overflow", nodecl.}
  func subOverflow*[T: OverflowArithmeticTypes](a, b: T,
      result: ptr T): bool {.importc: "__builtin_sub_overflow", nodecl.}

  const hasPrefetch* = true
  func prefetchIntrinsic(memory: pointer, rw,
      locality: static int) {.importc: "__builtin_prefetch", nodecl.}

  type
    PrefetchLocality* {.pure.} = enum
      High         ## Data will be gone from cache soon
      Medium       ## Rather high than extremely low
      Low          ## Rather extermely low than high
      ExtremelyLow ## Data will stay in cache for a while

  template prefetch*(memory: pointer, writeExpected: static bool = false,
      locality: static PrefetchLocality = ExtremelyLow): untyped =
    ## Mechanism to prefetch data that is expected to be used soon
    ## For example one could schedule cache prefetch for data that will be used in next iteration
    ## Should be used with extreme cautiousness, as misuse could lead to abysmal results
    prefetchIntrinsic memory, writeExpected.int, locality.int

  const hasUnreachable* = true
  func unreachable* {.importc: "__builtin_unreachable", nodecl, noreturn.}
    ## Should be used to instruct compiler that particular execution branch is not reachable

  const hasAssumeAligned* = true
  func assumeAligned*[T](memory: ptr T, alignment: static csize): ptr T {.importc: "__builtin_assume_aligned", nodecl.}

  # todo:
  # https://www.open-std.org/jtc1/sc22/wg14/www/docs/n1169.pdf
  # https://gcc.gnu.org/onlinedocs/gcc/Fixed-Point.html#Fixed-Point

else:
  const
    hasOverflowArithmetic* = false
    hasPrefetch* = false
    hasUnreachable* = false
    hasAssumeAligned* = false
