## Efficient and portable saturated arithmetic implementation.
##
## Based on:
## http://locklessinc.com/articles/sat_arithmetic/
## https://stackoverflow.com/questions/17580118/signed-saturated-add-of-64-bit-ints

import std/typetraits

import pcommon/intrinsics

# todo: There's compiler/saturate but its not exposed currently
# todo: Benchmark

{.push inline, overflowChecks: off.}

func `|+|`*[I: SomeUnsignedInt](a, b: I): I =
  when hasOverflowArithmetic and I is OverflowArithmeticTypes:
    if addOverflow(a, b, result.unsafeAddr):
      result = I.high
  else:
    let t = a + b
    t or ((t < a).I * I.high)

func `|+|`*[I: SomeSignedInt](a, b: I): I =
  when hasOverflowArithmetic and I is OverflowArithmeticTypes:
    if addOverflow(a, b, result.unsafeAddr):
      result = cast[I](cast[I.toUnsigned](b) shr (sizeof(I) * 8)) + I.high
  else:
    # todo: Potentialy quite bad
    if a > 0 and b > I.high - a:
      I.high
    elif a < 0 and b < I.low - a:
      I.low
    else:
      a + b

func `|-|`*[I: SomeUnsignedInt](a, b: I): I =
  when hasOverflowArithmetic and I is OverflowArithmeticTypes:
    if subOverflow(a, b, result.unsafeAddr):
      result = I.low
  else:
    let t = a - b
    t and ((t <= a).I * I.high)

func `|-|`*[I: SomeSignedInt](a, b: I): I =
  when hasOverflowArithmetic and I is OverflowArithmeticTypes:
    if subOverflow(a, b, result.unsafeAddr):
      result = if b < 0: I.high else: I.low
  else:
    # todo: Potentialy quite bad
    if a >= 0 and b <= I.low + a:
      I.high
    elif a < 0 and b > I.high + a:
      I.low
    else:
      a - b

{.pop.}
