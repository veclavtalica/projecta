import std/[macros, typetraits]

proc replaceAll(body, name, wth: NimNode) =
  for i, x in body:
    if x.kind == nnkIdent and name.eqIdent x:
      body[i] = wth
    else:
      x.replaceAll(name, wth)

# todo: Comptime definition that would turn it off, which could be crucial for memory constrained targets.
template unrolledFor*(nameP, toUnroll, bodyP: untyped): untyped =
  mixin
    getType,
    newTree,
    NimNodeKind,
    `[]`,
    add,
    newIdentDefs,
    newEmptyNode,
    newStmtList,
    newLit,
    replaceAll,
    copyNimTree

  macro myInnerMacro(name, body: untyped) {.gensym.} =
    let typ = getType(typeof(toUnroll))
    result = nnkBlockStmt.newTree(newEmptyNode(), newStmtList())
    result[^1].add nnkVarSection.newTree(newIdentDefs(name, typ[^1]))
    for x in toUnroll:
      let myBody = body.copyNimTree()
      myBody.replaceAll(name, newLit(x))
      result[^1].add myBody

  myInnerMacro(nameP, bodyP)
