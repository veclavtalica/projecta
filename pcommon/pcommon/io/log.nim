import std/[strformat, strutils, terminal]

import pcommon/private/annotations

# todo: Timestamps
# todo: Different levels, which reduce verbosity for particular builds
# todo: Use nim-faststreams
# todo: Use static buffer for writing formatted output
#       This will also help a lot in case of non-buffered files

var outfile: File = stdout
var supportsColor: bool = true

# todo: Receive lines as body
proc segment*(title: static string, lines: varargs[string, `$`]) {.clean.} =
  try:
    outfile.write(fmt"=== {title:^24} ===")
    outfile.write('\n')
    outfile.write(lines.join("\n"))
    outfile.write("\n--------------------------------\n")
    outfile.flushFile()
  except IOError:
    discard
  except ValueError:
    assert(false)

proc error*(line: string or cstring) {.clean.} =
  try:
    if supportsColor:
      outfile.styledWriteLine(fgRed, $line)
    else:
      outfile.write($line)
    outfile.write('\n')
    outfile.flushFile()
  except IOError:
    discard

proc message*(lines: varargs[string, `$`]) {.clean.} =
  try:
    outfile.write(lines.join(" "))
    outfile.write('\n')
    outfile.flushFile()
  except IOError:
    discard
