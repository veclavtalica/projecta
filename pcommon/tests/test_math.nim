import std/unittest

import pcommon/math/saturated

# todo: Test subrange types

suite "saturated":
  test "unsigned add":
    check(uint.high |+| 1 == uint.high)
    check(uint8.high |+| 2 == uint8.high)
    check(160u8 |+| 200u8 == uint8.high)
    check(0u16 |+| uint16.high == uint16.high)
    check(8u32 |+| 8u32 == 16u32)

  test "unsigned sub":
    check(uint.low |-| 3 == uint.low)
    check(uint8.low |-| 4 == uint8.low)
    check(4u16 |-| uint16.high == uint16.low)
    check(uint32.low |-| uint32.high == uint32.low)
    check(30u8 |-| 40u8 == uint8.low)
    check(40u8 |-| 10u8 == 30u8)

  test "signed add":
    check(int.high |+| 1 == int.high)
    check(int8.high |+| 2 == int8.high)
    check(120i8 |+| 100i8 == int8.high)
    check(0i16 |+| int16.high == int16.high)
    check(8i32 |+| 8i32 == 16i32)
    check(-8i32 |+| -8i32 == -16i32)
    check(-16i32 |+| 16i32 == 0i32)

  test "signed sub":
    check(int.low |-| 3 == int.low)
    check(int8.low |-| 4 == int8.low)
    check(-10i16 |-| int16.high == int16.low)
    check(int32.low |-| int32.high == int32.low)
    check(30i8 |-| 40i8 == -10i8)
    check(40i8 |-| 10i8 == 30i8)
    check(-40i8 |-| -10i8 == -30i8)
