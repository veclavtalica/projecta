import std/unittest

import pcommon/types/buffer
import pcommon/types/allocator

# todo: Test errors and defects
# todo: Test destruction of stored items

suite "buffer":
  test "basic":
    var buffer = initBuffer(NimAllocator, uint8)
    check(buffer.len == 0)
    buffer.ensureCapacity(100)
    check(buffer.capacity == 100)
    buffer.setLen(100)
    check(buffer.len == 100)
    for i, v in buffer.mpairs:
      v = i.uint8
    check(buffer[99] == 99)

  test "range":
    var buffer = initBuffer(NimAllocator, range[0..100])
    check(buffer.len == 0)
    buffer.ensureCapacity(100)
    check(buffer.capacity == 100)
    buffer.setLen(100)
    check(buffer.len == 100)
    for i, v in buffer.mpairs:
      v = i
    check(buffer[99] == 99)

  test "move":
    var buffer = initBuffer(NimAllocator, uint8, capacity = 32)
    buffer.setLen(1)
    buffer[0] = 8
    var moved = buffer.move()
    check(moved[0] == 8)
    check(buffer.capacity == 0)
    check(moved.capacity == 32)
