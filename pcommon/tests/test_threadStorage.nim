import unittest

import pcommon/threadStorage

# todo: Test mutliple threads
# todo: Test between call storage
# todo: Test items with destructors
# todo: Test zeroing

suite "threadTempStorage":
  test "sequence":
    var address: uint
    withThreadBuffer(bar, uint16, 16):
      bar.add 36u16
      address = cast[uint](bar[0].addr)
    withThreadBuffer(bar, uint8, 32):
      bar.setLen 1
      check(bar[0] == 36)
      check(address == cast[uint](bar[0].addr))
    shrinkThreadStorage()

  test "after shrinking":
    var address: uint
    withThreadBuffer(bar, uint64, 4):
      bar.add 0u64
      address = cast[uint](bar[0].addr)
    withThreadBuffer(bar, uint8, 32):
      bar.setLen 1
      check(bar[0] == 0)
      check(address == cast[uint](bar[0].addr))
    shrinkThreadStorage()

  test "nested":
    withThreadBuffer(foo, uint8, 64):
      foo.add 255u8
      # This address should remain between allocations, as they're reused.
      var address: uint
      withThreadBuffer(bar, float, 16):
        bar.setLen 1
        address = cast[uint](bar[0].addr)
      withThreadBuffer(bar, uint8, 64):
        bar.setLen 1
        check(address == cast[uint](bar[0].addr))
        check(foo[0] == 255)
    shrinkThreadStorage()

  test "object ptr":
    withThreadObject(foo, uint8):
      foo[] = 3
      check(foo[] == 3)
    shrinkThreadStorage()

  test "zeroing":
    withThreadBuffer(bar, uint16, 16):
      for i in 0..<16:
        bar.add i.uint16
    withThreadBufferZero(bar, uint8, 32):
      bar.setLen(32)
      for v in bar:
        check(v == 0)
    shrinkThreadStorage()
