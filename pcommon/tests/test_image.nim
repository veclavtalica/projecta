import std/unittest

import pcommon/types/image

suite "image":
  test "gif loading":
    let rgba_float = image.load(NimAllocator, ColorRgba[float], "tests/res/test.gif")
    check(rgba_float.dimensions == (16u, 16u))
