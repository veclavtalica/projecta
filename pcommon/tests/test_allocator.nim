import std/unittest

import pcommon/types/allocator

# todo: Test errors and defects

template checkCompiles(body): untyped =
  check(compiles(body))

template checkNotCompiles(body): untyped =
  check(not compiles(body))

template testAllocator(allocator: typedesc[Allocator]): untyped =
  test "basic":
    var mem = initAllocation(allocator, uint8, 16)
    mem[15] = 32
    check(mem[15] == 32)

  test "resize":
    var mem = initAllocation(allocator, float, 8)
    mem[7] = 5.0f
    mem.resize(16)
    check(mem[7] == 5.0f)

  test "zero":
    var mem = initAllocation(allocator, uint8, 16, doZero = true)
    check(mem[15] == 0)
    mem[15] = 32
    mem.resizeZero(32)
    check(mem[31] == 0)
    check(mem[15] == 32)

  test "span":
    var mem = initAllocation(allocator, uint16, 16)
    var span = mem.asReadWriteSpan()
    check(span.len - 1 == mem.len - 1)
    span[span.len - 1] = uint16.high
    check(span[span.len - 1] == span[uint mem.len - 1])

  # todo: Cant make Nim work here
  # test "view invalidation":
  #   var mem = createZero(t[uint8], 16)
  #   var view = mem.asReadWriteSpan()
  #   view[0] = 8
  #   checkNotCompiles:
  #     mem.resize(32)
  #     view[0] = 0 # This view might point to invalid memory after resize
  #   check(view[0] == 8)

suite "nim thread allocator":
  testAllocator NimAllocator

suite "nim global allocator":
  testAllocator NimGlobalAllocator

# type TestAllocator[T] = object
# proc free(t: typedesc[TestAllocator], a: var Allocation[t]) =
#   raise newException(CatchableError, "Destroyed")

# suite "custom allocator":
#   test "destroy":
#     expect(CatchableError):
#       block:
#         # Dummy work so that optimizer will not get rid of that
#         var arr = [0u8, 1]
#         var mem = initAllocation(TestAllocator, uint8, cast[ptr UncheckedArray[uint8]](arr[0].addr), arr.len)
#         mem[0] = 1
#         mem[1] = 0
#         `=destroy`(mem)
