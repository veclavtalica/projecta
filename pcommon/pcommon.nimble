# Package

version       = "0.1.0"
author        = "Veclav Talica"
description   = "projecta[common]"
license       = "GPL-3.0-only"
srcDir        = "./"
skipDirs      = @["tests"]

# Dependencies

requires "nim >= 1.6.8"

# todo: Should only be requires if image loading is used
requires "stb_image >= 2.5"
