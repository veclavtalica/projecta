
const submodules = ["pcommon", "pprocgen", "pgraphics", "pwindow"]

template cd(dir: string, body: untyped): untyped =
  let prevDir = getCurrentDir()
  cd dir
  body
  cd prevDir

task install, "Install every submodule via nimble":
  for submodule in submodules:
    cd submodule:
      exec "nimble install -y --silent"

task test, "Test every submodule via nimble":
  for submodule in submodules:
    cd submodule:
      exec "nimble test --silent"

task build, "Build pgame":
  cd "pgame":
    exec "nimble build --silent"
