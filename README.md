# projecta
Collection of Nim modules that i intend to reuse.

Also it has my games in it.

## Various resources
[Combining several GL draw calls into one: a rounded rectangle](https://mortoray.com/combining-several-gl-draw-calls-into-one-a-rounded-rectangle/)

[High-Throughput Game Message Server with Python websockets](https://mortoray.com/high-throughput-game-message-server-with-python-websockets/)

[WebGL best practices](https://developer.mozilla.org/en-US/docs/Web/API/WebGL_API/WebGL_best_practices)

## Ideas to consider
### OpenGL Performance Benchmark
Suite of tests that collects data about performance characteristics around alternative ways of doing things.

For example, whether clearing depth buffer with stencil buffer at the same time is faster, or blocking until rendering is done.

Using different texture formats for data loading could also differ in performance.
