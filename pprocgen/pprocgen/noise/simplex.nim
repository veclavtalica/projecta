import ./private/fastNoiseLite

import pcommon/types/[vec, rect, buffer]
export vec, rect, buffer

# todo: We need perlin/simplex algorithm that operates over fixed point values,
#       as they're faster to work when working with uniformed grids

# todo: Expose other algorithms
type
  NoiseGenerator*[T: SomeNumber] = object
    state: fastNoiseLite.state

func initNoiseGenerator*(t: typedesc[SomeNumber]): NoiseGenerator[t] =
  ## Needs to be called from every thread besdies main one that wishes to use this module
  NoiseGenerator[t](state: createState())

# todo: Check whether p could be sanely translated to cfloat
proc point*[T, S](a: var NoiseGenerator[T], p: Vec2[S]): T {.inline.} =
  when T is SomeFloat:
    T getNoise2D(a.state.addr, p.x.cfloat, p.y.cfloat)
  else:
    # todo: Put warning if T.high is above reasonable for cfloat
    T getNoise2D(a.state.addr, p.x.cfloat, p.y.cfloat) * T.high.cfloat

# todo: Check sanity, especially those conversions to ints
proc fill*[A, T, S](a {.noalias.}: var NoiseGenerator[T],
                    v {.noalias.}: var Buffer[A, T],
                    area: Rect2[S],
                    step: S = 1.S) =
  ## For A.T as float output is normalized to [0..1]
  ## For ordinals = [0..T.high]
  when S is SomeInteger:
    let stepsX = uint area.extend.width div step
    let stepsY = uint area.extend.height div step
  elif S is SomeFloat:
    let stepsX = uint (area.extend.width / step).trunc
    let stepsY = uint (area.extend.height / step).trunc

  v.setLen(stepsX * stepsY)

  var
    pos = (area.origin.x.cfloat, area.origin.y.cfloat)

  for h in 0..<stepsY:
    for w in 0..<stepsX:
      v[w + h * stepsX] = a.point(pos)
      pos.x += step.cfloat
    pos.y += step.cfloat

proc fill*[T, S](a: var NoiseGenerator[T],
              allocator: typedesc[Allocator],
              area: Rect2[S],
              step: S = 1.S): Buffer[allocator, T] =
  fill(result, area, step)
