import std/math

# todo: Approximation table for sin and cos operations?

func sphereMeshVerticesRequired*(divisions: int): int = divisions * divisions * 3

template generateSphereVertices(offset: int = 0) {.dirty.} =
  let hd = h / divisions
  let vd = v / divisions
  result[v + h * divisions + 0 + offset] = sin(PI * hd) * cos(2 * PI * vd)
  result[v + h * divisions + 1 + offset] = sin(PI * hd) * sin(2 * PI * vd)
  result[v + h * divisions + 2 + offset] = cos(PI * hd)

func generateSphereMesh*(divisions: int): seq[float32] =
  result.setLen(sphereMeshVerticesRequired(divisions))
  for h in 0..<divisions:
    for v in 0..<verticalLines:
      generateSphereVertices()

func fillSphereMesh*(buffer: var seq[float32], offset, divisions: int) =
  assert buffer.len >= sphereMeshVerticesRequired(divisions)
  for h in 0..<divisions:
    for v in 0..<verticalLines:
      generateSphereVertices(offset)
