## Indexed quad mesh

# todo: Non-indexed one?

import pcommon/types/[vec, buffer]
export vec, buffer

const
  verticesPerQuad* = 4.uint
  indicesPerQuad* = 6.uint

func generateQuadMeshImpl[T](v: var array[verticesPerQuad, Vec2[T]],
                          offset: Vec2[T],
                          scaling: T) {.inline.} =
  ## Order of points:
  ## 0 - 1
  ## | / |
  ## 2 - 3
  v[0] = ((offset.x + 0) * scaling, (offset.y + 0) * scaling)
  v[1] = ((offset.x + 1) * scaling, (offset.y + 0) * scaling)
  v[2] = ((offset.x + 0) * scaling, (offset.y + 1) * scaling)
  v[3] = ((offset.x + 1) * scaling, (offset.y + 1) * scaling)

func generateQuadMeshVertices*[T: SomeFloat](v: var array[verticesPerQuad, Vec2[T]],
                          offset: Vec2[T] = initVec2(-0.5.T, -0.5.T),
                          scaling: T = 1) {.inline.} =
  generateQuadMeshImpl(v, offset, scaling)

func generateQuadMeshVertices*[T: SomeUnsignedInt](v: var array[verticesPerQuad, Vec2[T]],
                          offset: Vec2[T] = Vec2[T].default,
                          scaling: T = 1) {.inline.} =
  generateQuadMeshImpl(v, offset, scaling)

func generateQuadMeshIndices*[T](v: var array[indicesPerQuad, T], startingQuad = 0.T) {.inline.} =
  # todo: Could ordering of them matter for rendering? Technically it could affect cache
  ## Order of indices, clock-wise:
  ## 0 2 1 1 2 3
  v[0] = startingQuad * verticesPerQuad + 0
  v[1] = startingQuad * verticesPerQuad + 2
  v[2] = startingQuad * verticesPerQuad + 1
  v[3] = startingQuad * verticesPerQuad + 1
  v[4] = startingQuad * verticesPerQuad + 2
  v[5] = startingQuad * verticesPerQuad + 3
