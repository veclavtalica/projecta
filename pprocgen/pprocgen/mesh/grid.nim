
# todo: Support for normalized integer output

import pcommon/types/[vec, span, buffer]
export vec, span, buffer
import pprocgen/mesh/quad
export quad

# todo: Non-overlapping version
# todo: Receive offset optional parameter?
proc generateGridMeshVertices*[T: SomeNumber](span: WriteSpan[Vec2[T]],
    dimensions: Vec2[uint], scaling = 1.T) =
  doAssert span.len >= dimensions.area * verticesPerQuad

  when T is SomeFloat:
    # For signed types - try to make mesh centered on origin,
    # so that scaling would work without need to translate
    var offset = initVec2[T](-dimensions.width.T / 2, -dimensions.height.T / 2)

  # todo: We dont need to increment pos.y
  for i, pos in dimensions.coords:
    generateQuadMeshVertices(
      span.asArrayPtr(
        start = verticesPerQuad * i,
        extend = verticesPerQuad)[],
      offset, scaling)

    offset.x += scaling
    if pos.x == dimensions.width - 1:
      offset.y += scaling
      offset.x = -dimensions.width.T / 2

proc generateGridMeshIndices*[T: SomeNumber](buffer: WriteSpan[T], count: uint,
    startingQuad = 0.T) =
  doAssert buffer.len >= indicesPerQuad * count
  for i in 0..<count:
    generateQuadMeshIndices(
      buffer.asArrayPtr(start = indicesPerQuad * i, extend = indicesPerQuad)[], i.T)

proc generateGridMeshVertices*[A: Allocator, T: SomeNumber](buffer: var Buffer[
    A, Vec2[T]], dimensions: Vec2[uint], scaling = 1.T) {.inline.} =
  buffer.setLen dimensions.area * verticesPerQuad
  generateGridMeshVertices buffer.asWriteSpan(), dimensions, scaling

proc generateGridMeshIndices*[A: Allocator, T: SomeNumber](buffer: var Buffer[
    A, T], count: uint, startingQuad = 0.T) {.inline.} =
  buffer.setLen indicesPerQuad * count
  generateGridMeshIndices buffer.asWriteSpan(), count, startingQuad
