
type
  Generator* = concept e
    type I = auto
    e.get(typedesc[I]) is I
