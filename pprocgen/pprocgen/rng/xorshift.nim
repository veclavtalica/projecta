import std/times

type
  Xorshift64* = object
    state: uint64

func initXorshift64*(seed: uint64): Xorshift64 =
  Xorshift64(state: seed)

proc initXorshift64*: Xorshift64 =
  Xorshift64(state: getTime().nanosecond.uint64)

func next(a: var Xorshift64): uint64 =
  var x = a.state
  x = x xor (x shl 13)
  x = x xor (x shr 17)
  x = x xor 5
  a.state = x
  x

func get*(a: var Xorshift64, T: typedesc[Ordinal]): T {.inline.} =
  when T is bool:
    a.next() < static (uint64.high div (bool.high.uint64 + 1))
  elif T is SomeFloat:
    # todo: Prevent bitmask resulting in INF and NAN
    {.fatal: "Unimplemented".}
  else:
    cast[T](a.next() mod (T.high.uint64 + 1) + T.low.uint64)
