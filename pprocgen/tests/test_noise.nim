import unittest

import pcommon/types/allocator
import pprocgen/noise/simplex

suite "noise":
  test "point [float]":
    var n = initNoiseGenerator(float)
    let a = n.point((0.0, 0.0))
    let b = n.point((0.0, 0.0))
    check a == b

  test "area [float]":
    var n = initNoiseGenerator(float)
    let rect = initRect2((0, 0), (64, 64))
    var a = initBuffer(NimAllocator, float, 0)
    var b = initBuffer(NimAllocator, float, 0)
    n.fill(a, rect)
    n.fill(b, rect)
    for i in 0..<a.len:
      check a[i] == b[i]

  test "area [uint8]":
    var n = initNoiseGenerator(uint8)
    let rect = initRect2((0, 0), (64, 64))
    var a = initBuffer(NimAllocator, uint8, 0)
    var b = initBuffer(NimAllocator, uint8, 0)
    n.fill(a, rect)
    n.fill(b, rect)
    for i in 0..<a.len:
      check a[i] == b[i]
