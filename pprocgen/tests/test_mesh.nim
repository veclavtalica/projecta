import unittest

import pprocgen/mesh/quad
import pprocgen/mesh/grid

suite "mesh":
  test "quad [float]":
    var quad: array[verticesPerQuad, Vec2[float]]
    generateQuadMeshVertices(quad)
    check(quad[0] == (-0.5, -0.5))
    check(quad[1] == ( 0.5, -0.5))
    check(quad[2] == (-0.5,  0.5))
    check(quad[3] == ( 0.5,  0.5))
    var indices: array[6, uint]
    generateQuadMeshIndices(indices)
    check(indices == [0.uint, 2, 1, 1, 2, 3])

  test "quad [uint8]":
    var quad: array[verticesPerQuad, Vec2[uint8]]
    generateQuadMeshVertices(quad)
    check(quad[0] == (0u8, 0u8))
    check(quad[1] == (1u8, 0u8))
    check(quad[2] == (0u8, 1u8))
    check(quad[3] == (1u8, 1u8))
    var indices: array[6, uint]
    generateQuadMeshIndices(indices)
    check(indices == [0.uint, 2, 1, 1, 2, 3])

  test "grid [float]":
    const dims = (2.uint, 2.uint)
    var buf = initBuffer(NimAllocator, Vec2[float], verticesPerQuad * dims.area)
    generateGridMeshVertices(buf, dims)
    check(buf.asArrayPtr(0, 4)[] == [(-1.0, -1.0), (0.0, -1.0), (-1.0, 0.0), (0.0, 0.0)])
    check(buf.asArrayPtr(4, 4)[] == [( 0.0, -1.0), (1.0, -1.0), ( 0.0, 0.0), (1.0, 0.0)])
    check(buf.asArrayPtr(8, 4)[] == [(-1.0,  0.0), (0.0,  0.0), (-1.0, 1.0), (0.0, 1.0)])
    check(buf.asArrayPtr(12, 4)[] == [(0.0,  0.0), (1.0,  0.0), ( 0.0, 1.0), (1.0, 1.0)])
    var indices = initBuffer(NimAllocator, uint, indicesPerQuad * dims.area)
    generateGridMeshIndices(indices, dims.area)
    check(indices.asArrayPtr(0, 6)[] == [0.uint, 2, 1, 1, 2, 3])
    check(indices.asArrayPtr(6, 6)[] == [4.uint, 6, 5, 5, 6, 7])
    check(indices.asArrayPtr(12, 6)[] == [8.uint, 10, 9, 9, 10, 11])
    check(indices.asArrayPtr(18, 6)[] == [12.uint, 14, 13, 13, 14, 15])
