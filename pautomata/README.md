# projecta[automata]
Programming game about abandoned underground AI complex, that randomly started working again. It must preserve itself, while accomplishing preprogammed tasks.

Player entity is physically expressed in the world as AI core, which could only gather information by devices in its network.

AI tasks are centered around making vault/safe-haven for humans. Without any humans insight.

## Command structure
On basic level player interacts with network by issuing commands, that are then dispatched by automatons.

Each command is backed by a program, that dictates the execution flow.

Later new layer is introduced - some automatons without body would analyze the network and issue command automatically for other automatons.

## Types of information
- Visual. Propagated by electromagnetic radiation of around human visible specter and localized to single cell. We're cheating and using model that transmits 'virtual rays' to objects, which then give their response, instead of simulating propagation of waves (Plato interpretation).
- Temperature. Similar to visual, but distinct.
- Waves. Composed from radio and hearing range waves. Their events are 'point like', instead of being a continuous wave, meaning that they're recognizable immediately. They have geometric origin and propagating through matter up to certain depth.
- Visual identification. To know what certain clump of matter represent in 'human' like way there needs to be special and very valuable device.
- Audio identification. Similar to visual identification, but with waves that correspond to human hearing.

## Story details
One of grand reveals: you're trapped in rock formation floating in space, which is left after catastrophic event on some planet. Debris is all around you. After this realization preprogrammed tasks of preservation of facility could either be continued, pointlessly, or AI gets cautiousness and makes escape ship for itself.

Why is information presented in desktop windows and other human perception driven ways?
One variant is to state, that all events that take place are replayed and filled by some person, who received the AI core memory in the future. This way actions taken by player are 'possible branches' that could be taken, which are getting investigated to retrieve what happened with the planet.

Information restoration of events before AI got shutdown and lost its memory then could be one of goals of the game. At least story and thematic wise. Gameplay wise it could be optional AI memory drives scattered in the world and require you to hack them or restore broken memory, giving a challenge to overcome.

### Explanation of music
The music that player hears could be explained by 'Therapy' core, that maintains the stability of AI.

We need to make sure thought, that it costs virtually nothing, so that player would not have an incentive to turn it off, unless they want to.

## Cores
Extensions of AI, that grant new interactions.

They could be collected from other AIs in other complexes one could encounter.

### Creativity core
Opens the ability to create and manipulate art.

Notably, all music in game is in XM tracker format, which is editable from within the game itself.
Actually its probably better to introduce simplified track format of our own.

Instruments and samples would be scattered around the world, waiting to be collected.

## Data
Binary blobs are virtual and abstracted by 'files' which are described by meta information about them.

Only algorithms that are aware of their 'encoding' could allow piping them.

## Programming
Programs are composed from gadget expressions and statements, that are composed by blocks. Expressions are nestable, while statements are not. Each gadget has associated execution cost, making operations costlier on time or energy, which creates incentive to think about their usage.

Aesthetics of it are rather terse, with local variable binds being only one letter, with optional coloring. It has practical advantage of being more easily showable on platforms with smaller screens, such as phones.


## TODO
- [ ] Tilemap rendering
