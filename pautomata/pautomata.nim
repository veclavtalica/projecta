import pwindow/sdl2
import pgraphics/[gl, tilemap]
import pcommon/types/[image]

let window = initWindow("projecta[automata]", 640, 480)

window.makeCurrent()
gl.init()

let
  img = image.load(NimAllocator, ColorBgra[float], "res/atlas.gif")
  texture = initTexture(img, Texture2D)
  tilemap = initTilemap(NimAllocator, texture, (16u, 16u))

var
  windowHasResized = true

# todo: Generic gameloop interface

proc render =
  # todo: Not blocking until rendering is done might be detrimental for systems with shared memory
  #       for graphics device and CPU, such as laptops, we need to test that.
  window.swap()
  gl.clear ColorRgba[float](red: 0.1, green: 0.1, blue: 0.15, alpha: 1.0)
  once:
    tilemap.prepareGrid (64u, 64u)
  tilemap.render window.dimensions
  glFlush()

block gameloop:
  while true:
    for event in window.events:
      case event.kind:
      of Quit:
        break gameloop
      of Window:
        if event.window.event == Resized:
          windowHasResized = true
      else: discard
    render()
    windowHasResized = false
