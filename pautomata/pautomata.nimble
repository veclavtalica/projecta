# Package

version       = "0.1.0"
author        = "Veclav Talica"
description   = "projecta[automata]"
license       = "GPL-3.0-only"
installExt    = @["nim"]
bin           = @["pautomata"]
srcDir        = "./"
skipDirs      = @["tests"]

# Dependencies

requires "nim >= 1.6.8"

requires "pcommon", "pgraphics", "pwindow", "pprocgen"
