import pcommon/types/vec
import pprocgen/noise/simplex

func chunkPeak*(chunkPos: Vec2[uint32]): uint16 =
  simplex.point(uint16, chunkPos)
