## Low level sdl2 window wrapper

import pkg/sdl2

import pcommon/types/vec
import pcommon/io/log

include ./private/event

type
  Window* {.requiresInit.} = object
    windowHandle: WindowPtr
    glContext: GlContextPtr

proc `=copy`*(dest: var Window, source: Window) {.error.}

proc `=destroy`*(x: var Window) =
  if x.windowHandle != nil:
    x.windowHandle.destroy()
    x.windowHandle = nil
    log.message "Window is freed"
  if x.glContext != nil:
    x.glContext = nil

# todo: Option instead of exception
proc initWindow*(title: string, dimensions: Vec2[uint]): Window {.noInit.} =
  let windowHandle = createWindow(
    title,
    SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
    dimensions.width.cint, dimensions.height.cint,
    SDL_WINDOW_OPENGL or SDL_WINDOW_RESIZABLE)

  if windowHandle == nil:
    raise newException(CatchableError, "Cannot create SDL window")

  let glContext = windowHandle.glCreateContext()
  if glContext == nil:
    windowHandle.destroy()
    raise newException(CatchableError, "Cannot create GL context")

  Window(windowHandle: windowHandle, glContext: glContext)

proc makeCurrent*(window: Window) =
  if glMakeCurrent(window.windowHandle, window.glContext) != 0:
    log.error(sdl2.getError())

proc swap*(window: Window) =
  ## Swap opengl buffers.
  ## Note: It causes blocking, glFinish call is assumed.
  window.windowHandle.glSwapWindow()

proc dimensions*(window: Window): Vec2[uint] =
  var
    width {.noInit.}: cint
    height {.noInit.}: cint
  window.windowHandle.getSize(width, height)
  (width.uint, height.uint)

# todo: Only return events that are relevant for given window.
iterator events*(window: Window): Event =
  var event = sdl2.defaultEvent
  while pollEvent(event):
    yield Event(impl: event)
