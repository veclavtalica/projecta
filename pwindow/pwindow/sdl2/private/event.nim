type
  EventKind* {.pure.} = enum
    Unknown
    Quit

  Event* = object
    impl: sdl2.Event

func kind*(event: Event): EventKind =
  case event.impl.kind:
  of QuitEvent:
    EventKind.Quit
  else:
    EventKind.Unknown
