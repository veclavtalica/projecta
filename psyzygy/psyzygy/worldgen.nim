import pcommon/types/vec
import pprocgen/noise/simplex

# Expert judgment on markers to deter inadvertent human intrusion into the Waste Isolation Pilot Plant

## todo: One interesting consequence from meshing optimizations we utilize is that generation
##       will affect amount of rendering work, potentially drastically.
##       For example, having the least possible block count at surface chunk will provide less vertex count.
##       Also making sure that we never have two surface chunks at given X,Y but different Z.
##       So, making any given chunk position more 'leveled' is key.

## todo: Using integer coordinates for noise gen is wasteful, we could use multiples of the smallest float value.
##       Tho it would require additional multiplication op.

func chunkPeak*(chunkPos: Vec2[uint32]): uint16 =
  simplex.point(uint16, chunkPos)
