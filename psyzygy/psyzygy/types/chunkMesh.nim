
# https://developer.apple.com/library/archive/documentation/3DDrawing/Conceptual/OpenGLES_ProgrammingGuide/TechniquesforWorkingwithVertexData/TechniquesforWorkingwithVertexData.html

## todo: Based of LOD we need to store points as different types, as resolution diminishes
##       32x32x32 chunks would require every point to be at least 16 bits (or 15 when packed),
##       while distant 4x4x4 and smaller could be stored in 8 bits per point (or 6 when packed)
##       Packing with other data at the same time increases the benefit further.
##       But OpenGL 2.1 doesnt allow that.

## todo: We can assume that chunks and all blocks in them could only be seen from 1 face at sufficient distance
##       Close chunks that are not in the same plane as player view only face 3 faces visible
##       In-plane ones require calculations relative to player view to know which ones are visible or not, but it's still only 3 per block at maximum,
##       or 1-2 for huge meshed blocks.

## todo: We could store same oriented faces continuously in single region, which makes updating 
##       based on changed orientation towards view point more trivial.
##       But it's problematic for cases where we have uneven distribution of them because of visibility optimization.

## todo: Miniatures could be saved to disk after their generation.

## todo: On meshing of packedVolume we could store the visibility bytes into thread local array sized to chunk.
##       This will allow checking whether something has ability to be seen to begin with, which is
##       relevant for inner subvolumes.

## todo: Visibility test could be accomplished with thread local storage of penetrating vectors at every possible
##       point and direction. It should have depth of penetration, and whether there was empty space before.
##       Chunks that arent in the same plane as viewpoint could drop walking at first encountering of opaque block.
