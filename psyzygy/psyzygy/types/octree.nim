##
## @defgroup   OCTREE octree
##
## @brief      Highly specialized octree structure for pgame chunks.
##
## @author     Veclav Talica
## @date       2022
##
## Goals are to make it reasonably compact, while not losing lookup speed that much.
## It's accomplished by following considerations:
## - Leaf nodes do not store coordinates for items, as it could be infered by index
## - Subdivision is signaled by a bit that is punned into blockid/offset union
## - Recursive operations are implemented via loop
## 

import std/bitops

import pcommon/types/buffer
import pgame/types/blocks

# todo: Ability to init from existing buffer, problem is just that it would require instancing of Node buffer on caller side
# todo: If we use byte buffer, we could reuse 'coords' storage of fully saturated nodes, but it would require fighting for alignment

type
  Octree*[A] = object
    ## Packed octree with sentinel bitmask support for subrange types
    data: Buffer[A]

  BlockIdOrNodeOffset {.union.} = object
    offset: range[0..32767] ## Offset into buffer by Node or LeafNode
    blockId: BlockId

  Item {.packed.} = object
    blockIdOrNodeOffset {.bitsize:15.}: BlockIdOrNodeOffset
      ## If divided is set its node offset, otherwise blockid or nothing
    divided {.bitsize:1.}: bool

  Coords {.packed.} = object
    x {.bitsize:5.}: range[0..31]
    y {.bitsize:5.}: range[0..31]
    z {.bitsize:6.}: range[0..31]

  LeafNode = object
    ## Coordinates for each item are implied by their index
    items: array[8, Item]

  Node = object
    items: array[8, Item]
    coords: array[8, Coords]

static:
  doAssert(sizeof(Coords) == 2)
  doAssert(sizeof(Item) == 2)
  doAssert(sizeof(Coords) == sizeof(Item))
  doAssert(alignof(Node) == alignof(LeafNode))

func initOctree*(a: typedesc[Allocator]): Octree[a[uint8]] =
  result.data = initBuffer(a[uint8])
