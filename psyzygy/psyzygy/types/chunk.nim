import psyzygy/types/blocks
export blocks
import psyzygy/constants
export constants
import psyzygy/types/chunkStorage/[unit, packed]
export unit, packed

# todo: Varying 'background' blockId for over and underground.
#       While it's beneficial to assume empty space be predominant in overworld terrain, -
#       underground might better be inversed and defaulted to local rock formation instead.
#       It's predominantly beneficial for disk storage.

type
  StorageKind* {.pure.} = enum
    Unit
    Packed
    # Octree

  Chunk*[A: Allocator] {.requiresInit.} = object
    ## Polymorphic chunk storage
    ## Naive: Whole data is directly indexable by coordinates
    ## Packed: Greedy meshed volume, requires iteration to decode information
    ## Octree: Sparse tree, good compromise between lookup speed of naive and size of packed
    case storageType: StorageKind
    of Unit:
      storageNaive: UnitChunk[A]
    of Packed:
      storagePacked: ptr PackedChunk[A]
    # of Octree:
    #   storageOctree: ptr Octree[uint8, BlockId]
