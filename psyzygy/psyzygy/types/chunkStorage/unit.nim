import pcommon/types/[allocator, buffer, matrix]
import psyzygy/types/blocks
import psyzygy/constants
export constants, blocks, matrix

# todo: One potentially beneficial idea is cache friendly unit chunk storage.
#       Picture this: when you're iterating over X coordinates each value is immediately accessible,
#       but when you're walking by Z coordinate you need to step over 32 * 32 blocks, which isnt bad,
#       as it's sitll easily within range of L2 cache, but could be better.

# todo: Make it vary by dimensionality

type
  UnitChunk*[A: Allocator] {.requiresInit.} = object
    ## Full volume of chunk in linear fashion
    data: Allocation[A, BlockId]

  # todo:
  UnitPosition* = distinct uint16
    ## Packed position within a chunk, in contrast to a 3 element vector.
    ## It should be used in cases where absolute position matters, for indexing,
    ## instead of interspectable dimensions.

func initUnitChunk*[A](b: sink Buffer[A, BlockId]): UnitChunk[A] =
  assert b.len == ChunkVolume
  UnitChunk[A](data: b.stealAllocation())

template asMatrixPtr*[A](u: UnitChunk[A]): ptr Matrix3[initVec3(
    ChunkDimension.uint), BlockId] =
  cast[ptr Matrix3[initVec3(ChunkDimension.uint), BlockId]](
      u.data.asUncheckedArray())
