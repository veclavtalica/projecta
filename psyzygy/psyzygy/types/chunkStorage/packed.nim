import pcommon/types/[buffer, matrix, rect]
import pcommon/[threadStorage, utils]
import psyzygy/types/chunkStorage/unit # todo: Will create cyclic dependency if unit wants to use this representation
import psyzygy/types/blocks
import psyzygy/constants
export constants, blocks, rect

# todo: One problem with this implementation is that its unbalanced,
#       meaning that lookup is dependent on spacial position of target.
#       One can't just iterate from the end until point is found to be empty, but also
#       have to determine whether none of segments actually has it.

type
  PackedChunk*[A: Allocator] = object
    ## Greedy packed chunk data.
    ## It doesnt store empty segments, reducing the footprint, while making it faster to decode.
    segments*: Buffer[A, PackedSegment]

  # todo: For disk stored representation we could optimize size further by placing each origin and extend
  #       in vector of specific blockId.
  # todo: For disk saved / transferred via network items we could pack it as tightly as possible.
  # todo: We could use uint16 for origin and extends if we store it in the same way indexes are calculated.
  PackedSegment* = tuple
    blockId: BlockId
    area: Rect3[uint8]
      # Note that extend of area is in reversed coordinates compared to iteration directions.

template len*[A](a: PackedChunk[A]): uint = a.segments.len

template origin*(a: PackedSegment): Vec3 = a.area.origin
template extend*(a: PackedSegment): Vec3 = a.area.extend

# todo: Is it possible to reuse allocation in given unit chunk to construct the packed one?
# todo: Using uint instead of uint8 for computation could be better, as only saved data needs to be truncated to uint8
# todo: Will different A instantiations create multiples of this code? Could be problematic as it's huge
#       because of heavy inlining.
proc initPackedChunk*[A](allocator: typedesc[Allocator], chunk: UnitChunk[
    A]): PackedChunk[allocator] =
  withThreadBuffer(segments, PackedSegment, ChunkVolume):
    var
      index = 1.uint
      segmentCount = 0.uint
      curBlockId = chunk.asMatrixPtr[][0]
      curOrigin = Vec3[uint8].default
      xPrev, yPrev = segments.asUncheckedArray()[0].addr
        # Those will be formed before usage
      xMerged, yMerged = false
        # todo: Could be implied by branches taken

    template makeSegment(x, y, z) {.dirty.} =
      # todo: Will y != 0 check always be comptime?
      if y != 0 and xPrev[].origin.y != y:
        while xPrev[].origin.x < x: xPrev.inc
        if (xPrev[].origin.x == x and
            xPrev[].extend.x == x - curOrigin.x):
          xMerged = true
      if z != 0 and yPrev[].origin.z != z:
        while yPrev[].origin.xy < (x, y): yPrev.inc
        if (yPrev[].origin.xy == (x, y) and
            yPrev[].extend.xy == (x, y) - curOrigin.xy):
          yMerged = true

      if xMerged:
        xPrev[].blockId = BlockId.None # Mark as shadowed
        xMerged = false
        if yMerged:
          yPrev[].blockId = BlockId.None # Mark as shadowed
          segments.add (curBlockId, ((x, y, z), (x, y, z) - curOrigin + (0u8,
              1u8, 1u8)))
          segmentCount.dec
          yMerged = false
        else:
          segments.add (curBlockId, ((x, y, z), (x, y, z) - curOrigin + (0u8,
              1u8, 0u8)))
      elif yMerged:
        yPrev[].blockId = BlockId.None # Mark as shadowed
        segments.add (curBlockId, ((x, y, z), (x, y, z) - curOrigin + (0u8, 0u8, 1u8)))
        yMerged = false
      else:
        segments.add (curBlockId, ((x, y, z), (x, y, z) - curOrigin))
        segmentCount.inc

    template step(x, y, z) {.dirty.} =
      if chunk.asMatrixPtr[][index] != curBlockId:
        if curBlockId != BlockId.None:
          makeSegment(x, y, z)
        curBlockId = chunk.asMatrixPtr[][index]
        curOrigin = (x, y, z)
      index.inc

    for x in 1u8..<ChunkDimension.uint8:
      step x, 0u8, 0u8
    makeSegment 31u8, 0u8, 0u8

    for y in 1u8..<ChunkDimension.uint8:
      for x in 0u8..<ChunkDimension.uint8:
        step x, y, 0u8
      makeSegment 31u8, y, 0u8

    for z in 1u8..<ChunkDimension.uint8:
      for x in 0u8..<ChunkDimension.uint8:
        step x, 0u8, z
      makeSegment 31u8, 0u8, z

      for y in 1u8..<ChunkDimension.uint8:
        for x in 0u8..<ChunkDimension.uint8:
          step x, y, z
        makeSegment 31u8, y, z

    # todo: Would be better to not require this
    result.segments = initBuffer(allocator, PackedSegment, segmentCount)
    for v in segments:
      if v.blockId != BlockId.None:
        result.segments.add v
