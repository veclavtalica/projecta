
type
  BlockId* {.pure, size: sizeof(uint16).} = enum
    ## Represents block identifier and its enumeration of types
    None
      ## Sentinel to denote absence in data types
      # todo: Rename to 'Default', as it's planned to mean different thing depending on context.
    Air
    Crust
