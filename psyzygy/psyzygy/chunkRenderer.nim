from std/strutils import unindent

import pgraphics/gl
import pgraphics/gl/shader
import pcommon/options

# todo: Consider using occlusion queries for occluding meshes on GPU.
#       https://developer.nvidia.com/gpugems/gpugems2/part-i-geometric-complexity/chapter-6-hardware-occlusion-queries-made-useful
#       Doing this to lower res framebuffer could also make some sense.

# todo: One interesting way of going about chunk rendering is to compose them from all possible shapes via multi draw.
#       This works especially well with packed representations, as you could have 1 to 1 correlation with them this way,
#       without need to construct anything special for any given chunk, expect arrays for draw command.
#       Drawback is that you end up wasting quite a bit of space for all possible configurations of faces,
#       but also it's not quite clear how to assign additional information, such as uv, per vertex.

# todo: Lighting information for single extended face could be encoded by color index if we prepare texture
#       that has every possible combination of color + lighting in different sequencing. It's easily achievable
#       even by guaranteed minimum size of texture2d by opengl. cpu would have to spend some time mapping this information tho.
#       In fact for (32 x 32) x 256 (color depth) x 16 (light depth) texture of 2048 by 2048 exactly is required.
#       And for such size, gl float is more than enough to represent both x and y in single integer.
#       CPU mapping should be accomplished via bitmask operations, and only transformed to float at the very end.
#       Or, alternatively, via node tree look up, where colors and light level are searched recursively.

# todo: Previous technique could be enhanced by meshes that are composed not by blockId differences, but volumes.

# todo: Use ARB_shader_texture_lod and texture2DLod in vertex shader to retrieve single color in per vertex fashion.
#       Otherwise each pixel will require separate texture sampling, which is not cheap.

const
  closeupVertexShader = """
    #version 120

    attribute vec3 in_position;
    attribute float in_color_idx; // Shading is encoded here too, as palette has colors of different dimness.

    uniform mat4 u_projection;
    uniform mat4 u_modelview; // Only this is intended to be modified between individual chunk draw calls.

    varying float var_color_idx;

    void main() {
      gl_Position = u_projection * u_modelview * in_position;
      var_color_idx = in_color_idx;
    }

  """.unindent()
  closeupFragmentShader = """
    #version 120

    uniform sampler1D u_palette;

    varying float var_color_idx;

    void main() {
      gl_FragColor = texture1D(u_palette, var_color_idx);
    }

  """.unindent()

var closeupProgram: Shader
var closeupIndexBuffer: GlServerBuffer[GLuint]
  ## Shared EBO, as every chunk follows the same quad scheme.
  ## todo: We could reuse single index buffer for all meshes that are composed from quads, not just in chunk renderer.

# todo: One problem with this strategy is that it's not obvious when we should free these resources.
proc initSharedState =
  ## We intend to reuse resources between different tilemap instances,
  ## so they're kept in global variables that are instanced once
  once:
    closeupProgram = must initShader(closeupVertexShader, closeupFragmentShader)
    closeupIndexBuffer = initGlServerBuffer(GLuint, ElementBuffer)
