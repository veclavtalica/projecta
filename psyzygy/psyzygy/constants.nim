
const
  ChunkDimension* = 32
  ChunkFaceArea* = ChunkDimension * ChunkDimension
  ChunkVolume* = ChunkFaceArea * ChunkDimension
