import unittest

import pcommon/utils
import pcommon/types/buffer
import psyzygy/types/chunkStorage/[unit, packed]

# todo: Test unit chunk

suite "packedChunk":
  test "empty":
    let arr = array[ChunkVolume, BlockId].default
    let unit = initUnitChunk(initBuffer(arr.addr))
    let packed = initPackedChunk(NimAllocator, unit)
    check packed.len == 0

  test "full":
    let arr = initArrayOf[ChunkVolume, BlockId](BlockId.Crust)
    let unit = initUnitChunk(initBuffer(arr.addr))
    let packed = initPackedChunk(NimAllocator, unit)
    check packed.len == 1
    check packed.segments[0] == (BlockId.Crust, ((0u8, 0u8, 0u8), (31u8, 31u8, 31u8)))
