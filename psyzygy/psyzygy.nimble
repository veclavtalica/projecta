# Package

version       = "0.1.0"
author        = "Veclav Talica"
description   = "projecta[syzygy]"
license       = "GPL-3.0-only"
srcDir        = "./"
skipDirs      = @["tests"]
bin           = @["psyzygy"]
installExt    = @["nim"]

# Dependencies

requires "nim >= 1.6.8"

requires "pcommon", "pgraphics", "pwindow", "pprocgen"
