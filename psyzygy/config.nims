# todo: Direct compiler invocation produces smaller binary for whatever reason?

--experimental: strictFuncs

when defined(release):
  --nimcache: ".nimcache/release"
  --d: danger
  --opt: size
  --stackTrace: off
  --lineTrace: off
  --mm: orc
  --d: lto
else:
  --nimcache: ".nimcache/debug"
  --debugger: native
  # todo: incremental build setting form nims is broken, pass it from command line
  # --incremental:on
