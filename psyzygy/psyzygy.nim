import pwindow/sdl2
import pgraphics/gl
import pcommon/types/image
import pcommon/types/allocator

import pgame/chunkRenderer

let window = initWindow("life", 640, 480)

window.makeCurrent()
gl.init()
chunkRenderer.initShaders()

let img = image.load(NimAllocator[ColorRgba[float]], "res/smile.gif")
var texture = initTexture(img)

proc render =
  gl.clear(ColorRgba[float](red: 1.0, green: 0.0, blue: 0.0, alpha: 1.0))
  window.swap()

block gameloop:
  while true:
    for event in window.events():
      case event.kind:
      of Quit:
        break gameloop
      else: discard
    render()
